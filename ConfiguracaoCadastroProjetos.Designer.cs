﻿namespace WindowsFormsApplication1
{
    partial class ConfiguracaoCadastroProjetos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tabControlgruporefresh = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonprojetorefresh = new System.Windows.Forms.Button();
            this.buttonprojetosalvar = new System.Windows.Forms.Button();
            this.buttonprojetosair = new System.Windows.Forms.Button();
            this.buttonprojetoapagar = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonclassificacaorefresh = new System.Windows.Forms.Button();
            this.buttonclassificacaosalvar = new System.Windows.Forms.Button();
            this.buttonclassificacaosair = new System.Windows.Forms.Button();
            this.buttonclassificacaoapagar = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.buttongruporefresh = new System.Windows.Forms.Button();
            this.buttongruposalvar = new System.Windows.Forms.Button();
            this.buttongruposair = new System.Windows.Forms.Button();
            this.buttongrupoapagar = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonvideoreflesh = new System.Windows.Forms.Button();
            this.buttonvideosalvar = new System.Windows.Forms.Button();
            this.buttonvideosair = new System.Windows.Forms.Button();
            this.buttonvideoapagar = new System.Windows.Forms.Button();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.tabControlgruporefresh.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.tableLayoutPanel9.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel1.Controls.Add(this.tabControlgruporefresh, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(568, 411);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tabControlgruporefresh
            // 
            this.tabControlgruporefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlgruporefresh.Controls.Add(this.tabPage1);
            this.tabControlgruporefresh.Controls.Add(this.tabPage2);
            this.tabControlgruporefresh.Controls.Add(this.tabPage4);
            this.tabControlgruporefresh.Controls.Add(this.tabPage3);
            this.tabControlgruporefresh.Location = new System.Drawing.Point(3, 3);
            this.tabControlgruporefresh.Name = "tabControlgruporefresh";
            this.tabControlgruporefresh.SelectedIndex = 0;
            this.tabControlgruporefresh.Size = new System.Drawing.Size(547, 405);
            this.tabControlgruporefresh.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(539, 379);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Projeto";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.52431F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.47569F));
            this.tableLayoutPanel2.Controls.Add(this.dataGridView1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(533, 373);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(133, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(397, 337);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.buttonprojetorefresh, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.buttonprojetosalvar, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.buttonprojetosair, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.buttonprojetoapagar, 0, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(124, 125);
            this.tableLayoutPanel3.TabIndex = 6;
            // 
            // buttonprojetorefresh
            // 
            this.buttonprojetorefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonprojetorefresh.Location = new System.Drawing.Point(3, 93);
            this.buttonprojetorefresh.Name = "buttonprojetorefresh";
            this.buttonprojetorefresh.Size = new System.Drawing.Size(118, 29);
            this.buttonprojetorefresh.TabIndex = 6;
            this.buttonprojetorefresh.Text = "Refresh";
            this.buttonprojetorefresh.UseVisualStyleBackColor = true;
            this.buttonprojetorefresh.Click += new System.EventHandler(this.buttonprojetorefresh_Click);
            // 
            // buttonprojetosalvar
            // 
            this.buttonprojetosalvar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonprojetosalvar.Location = new System.Drawing.Point(3, 3);
            this.buttonprojetosalvar.Name = "buttonprojetosalvar";
            this.buttonprojetosalvar.Size = new System.Drawing.Size(118, 24);
            this.buttonprojetosalvar.TabIndex = 4;
            this.buttonprojetosalvar.Text = "Salvar";
            this.buttonprojetosalvar.UseVisualStyleBackColor = true;
            this.buttonprojetosalvar.Click += new System.EventHandler(this.buttonprojetosalvar_Click);
            // 
            // buttonprojetosair
            // 
            this.buttonprojetosair.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonprojetosair.Location = new System.Drawing.Point(3, 63);
            this.buttonprojetosair.Name = "buttonprojetosair";
            this.buttonprojetosair.Size = new System.Drawing.Size(118, 24);
            this.buttonprojetosair.TabIndex = 5;
            this.buttonprojetosair.Text = "Sair";
            this.buttonprojetosair.UseVisualStyleBackColor = true;
            this.buttonprojetosair.Click += new System.EventHandler(this.buttonprojetosair_Click);
            // 
            // buttonprojetoapagar
            // 
            this.buttonprojetoapagar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonprojetoapagar.Location = new System.Drawing.Point(3, 33);
            this.buttonprojetoapagar.Name = "buttonprojetoapagar";
            this.buttonprojetoapagar.Size = new System.Drawing.Size(118, 24);
            this.buttonprojetoapagar.TabIndex = 2;
            this.buttonprojetoapagar.Text = "Apagar";
            this.buttonprojetoapagar.UseVisualStyleBackColor = true;
            this.buttonprojetoapagar.Click += new System.EventHandler(this.buttonprojetoapagar_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(539, 379);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Classificacao";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.52431F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.47569F));
            this.tableLayoutPanel4.Controls.Add(this.dataGridView2, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(533, 373);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(133, 3);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(397, 337);
            this.dataGridView2.TabIndex = 0;
            this.dataGridView2.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellEndEdit);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.buttonclassificacaorefresh, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.buttonclassificacaosalvar, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.buttonclassificacaosair, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.buttonclassificacaoapagar, 0, 1);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(124, 122);
            this.tableLayoutPanel5.TabIndex = 6;
            // 
            // buttonclassificacaorefresh
            // 
            this.buttonclassificacaorefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonclassificacaorefresh.Location = new System.Drawing.Point(3, 93);
            this.buttonclassificacaorefresh.Name = "buttonclassificacaorefresh";
            this.buttonclassificacaorefresh.Size = new System.Drawing.Size(118, 26);
            this.buttonclassificacaorefresh.TabIndex = 6;
            this.buttonclassificacaorefresh.Text = "Refresh";
            this.buttonclassificacaorefresh.UseVisualStyleBackColor = true;
            this.buttonclassificacaorefresh.Click += new System.EventHandler(this.buttonclassificacaorefresh_Click);
            // 
            // buttonclassificacaosalvar
            // 
            this.buttonclassificacaosalvar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonclassificacaosalvar.Location = new System.Drawing.Point(3, 3);
            this.buttonclassificacaosalvar.Name = "buttonclassificacaosalvar";
            this.buttonclassificacaosalvar.Size = new System.Drawing.Size(118, 24);
            this.buttonclassificacaosalvar.TabIndex = 4;
            this.buttonclassificacaosalvar.Text = "Salvar";
            this.buttonclassificacaosalvar.UseVisualStyleBackColor = true;
            this.buttonclassificacaosalvar.Click += new System.EventHandler(this.buttonclassificacaosalvar_Click);
            // 
            // buttonclassificacaosair
            // 
            this.buttonclassificacaosair.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonclassificacaosair.Location = new System.Drawing.Point(3, 63);
            this.buttonclassificacaosair.Name = "buttonclassificacaosair";
            this.buttonclassificacaosair.Size = new System.Drawing.Size(118, 24);
            this.buttonclassificacaosair.TabIndex = 5;
            this.buttonclassificacaosair.Text = "Sair";
            this.buttonclassificacaosair.UseVisualStyleBackColor = true;
            this.buttonclassificacaosair.Click += new System.EventHandler(this.buttonclassificacaosair_Click);
            // 
            // buttonclassificacaoapagar
            // 
            this.buttonclassificacaoapagar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonclassificacaoapagar.Location = new System.Drawing.Point(3, 33);
            this.buttonclassificacaoapagar.Name = "buttonclassificacaoapagar";
            this.buttonclassificacaoapagar.Size = new System.Drawing.Size(118, 24);
            this.buttonclassificacaoapagar.TabIndex = 2;
            this.buttonclassificacaoapagar.Text = "Apagar";
            this.buttonclassificacaoapagar.UseVisualStyleBackColor = true;
            this.buttonclassificacaoapagar.Click += new System.EventHandler(this.buttonclassificacaoapagar_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.tableLayoutPanel8);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(539, 379);
            this.tabPage4.TabIndex = 5;
            this.tabPage4.Text = "Grupo";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.52431F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.47569F));
            this.tableLayoutPanel8.Controls.Add(this.dataGridView4, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel9, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(539, 379);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // dataGridView4
            // 
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView4.Location = new System.Drawing.Point(135, 3);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(401, 343);
            this.dataGridView4.TabIndex = 0;
            this.dataGridView4.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView4_CellEndEdit);
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.buttongruporefresh, 0, 3);
            this.tableLayoutPanel9.Controls.Add(this.buttongruposalvar, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.buttongruposair, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.buttongrupoapagar, 0, 1);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 4;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(124, 125);
            this.tableLayoutPanel9.TabIndex = 6;
            // 
            // buttongruporefresh
            // 
            this.buttongruporefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttongruporefresh.Location = new System.Drawing.Point(3, 93);
            this.buttongruporefresh.Name = "buttongruporefresh";
            this.buttongruporefresh.Size = new System.Drawing.Size(118, 29);
            this.buttongruporefresh.TabIndex = 6;
            this.buttongruporefresh.Text = "Refresh";
            this.buttongruporefresh.UseVisualStyleBackColor = true;
            this.buttongruporefresh.Click += new System.EventHandler(this.buttongruporefresh_Click);
            // 
            // buttongruposalvar
            // 
            this.buttongruposalvar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttongruposalvar.Location = new System.Drawing.Point(3, 3);
            this.buttongruposalvar.Name = "buttongruposalvar";
            this.buttongruposalvar.Size = new System.Drawing.Size(118, 24);
            this.buttongruposalvar.TabIndex = 4;
            this.buttongruposalvar.Text = "Salvar";
            this.buttongruposalvar.UseVisualStyleBackColor = true;
            this.buttongruposalvar.Click += new System.EventHandler(this.buttongruposalvar_Click);
            // 
            // buttongruposair
            // 
            this.buttongruposair.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttongruposair.Location = new System.Drawing.Point(3, 63);
            this.buttongruposair.Name = "buttongruposair";
            this.buttongruposair.Size = new System.Drawing.Size(118, 24);
            this.buttongruposair.TabIndex = 5;
            this.buttongruposair.Text = "Sair";
            this.buttongruposair.UseVisualStyleBackColor = true;
            this.buttongruposair.Click += new System.EventHandler(this.buttongruposair_Click);
            // 
            // buttongrupoapagar
            // 
            this.buttongrupoapagar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttongrupoapagar.Location = new System.Drawing.Point(3, 33);
            this.buttongrupoapagar.Name = "buttongrupoapagar";
            this.buttongrupoapagar.Size = new System.Drawing.Size(118, 24);
            this.buttongrupoapagar.TabIndex = 2;
            this.buttongrupoapagar.Text = "Apagar";
            this.buttongrupoapagar.UseVisualStyleBackColor = true;
            this.buttongrupoapagar.Click += new System.EventHandler(this.buttongrupoapagar_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tableLayoutPanel6);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(539, 379);
            this.tabPage3.TabIndex = 4;
            this.tabPage3.Text = "Video";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.52431F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.47569F));
            this.tableLayoutPanel6.Controls.Add(this.dataGridView3, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel7, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(533, 373);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.Location = new System.Drawing.Point(133, 3);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(397, 337);
            this.dataGridView3.TabIndex = 0;
            this.dataGridView3.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView3_CellEndEdit);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.buttonvideoreflesh, 0, 3);
            this.tableLayoutPanel7.Controls.Add(this.buttonvideosalvar, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.buttonvideosair, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.buttonvideoapagar, 0, 1);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 4;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(124, 126);
            this.tableLayoutPanel7.TabIndex = 6;
            // 
            // buttonvideoreflesh
            // 
            this.buttonvideoreflesh.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonvideoreflesh.Location = new System.Drawing.Point(3, 93);
            this.buttonvideoreflesh.Name = "buttonvideoreflesh";
            this.buttonvideoreflesh.Size = new System.Drawing.Size(118, 30);
            this.buttonvideoreflesh.TabIndex = 6;
            this.buttonvideoreflesh.Text = "Reflesh";
            this.buttonvideoreflesh.UseVisualStyleBackColor = true;
            this.buttonvideoreflesh.Click += new System.EventHandler(this.buttonvideoreflesh_Click);
            // 
            // buttonvideosalvar
            // 
            this.buttonvideosalvar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonvideosalvar.Location = new System.Drawing.Point(3, 3);
            this.buttonvideosalvar.Name = "buttonvideosalvar";
            this.buttonvideosalvar.Size = new System.Drawing.Size(118, 24);
            this.buttonvideosalvar.TabIndex = 4;
            this.buttonvideosalvar.Text = "Salvar";
            this.buttonvideosalvar.UseVisualStyleBackColor = true;
            this.buttonvideosalvar.Click += new System.EventHandler(this.buttonvideosalvar_Click);
            // 
            // buttonvideosair
            // 
            this.buttonvideosair.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonvideosair.Location = new System.Drawing.Point(3, 63);
            this.buttonvideosair.Name = "buttonvideosair";
            this.buttonvideosair.Size = new System.Drawing.Size(118, 24);
            this.buttonvideosair.TabIndex = 5;
            this.buttonvideosair.Text = "Sair";
            this.buttonvideosair.UseVisualStyleBackColor = true;
            this.buttonvideosair.Click += new System.EventHandler(this.buttonvideosair_Click);
            // 
            // buttonvideoapagar
            // 
            this.buttonvideoapagar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonvideoapagar.Location = new System.Drawing.Point(3, 33);
            this.buttonvideoapagar.Name = "buttonvideoapagar";
            this.buttonvideoapagar.Size = new System.Drawing.Size(118, 24);
            this.buttonvideoapagar.TabIndex = 2;
            this.buttonvideoapagar.Text = "Apagar";
            this.buttonvideoapagar.UseVisualStyleBackColor = true;
            this.buttonvideoapagar.Click += new System.EventHandler(this.buttonvideoapagar_Click);
            // 
            // ConfiguracaoCadastroProjetos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 411);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ConfiguracaoCadastroProjetos";
            this.Text = "Cadastro de Projetos";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tabControlgruporefresh.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.tableLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TabControl tabControlgruporefresh;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button buttonprojetosalvar;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Button buttonprojetosair;
        private System.Windows.Forms.Button buttonprojetoapagar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Button buttonclassificacaosalvar;
        private System.Windows.Forms.Button buttonclassificacaosair;
        private System.Windows.Forms.Button buttonclassificacaoapagar;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Button buttonvideosalvar;
        private System.Windows.Forms.Button buttonvideosair;
        private System.Windows.Forms.Button buttonvideoapagar;
        private System.Windows.Forms.Button buttonprojetorefresh;
        private System.Windows.Forms.Button buttonclassificacaorefresh;
        private System.Windows.Forms.Button buttonvideoreflesh;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button buttongruporefresh;
        private System.Windows.Forms.Button buttongruposalvar;
        private System.Windows.Forms.Button buttongruposair;
        private System.Windows.Forms.Button buttongrupoapagar;
    }
}