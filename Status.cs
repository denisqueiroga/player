﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Status : System.Windows.Forms.Form
    {
        string senha;
        public Status()
        {
            InitializeComponent();
            label_IPDB.Text = Usuario.IPDB;
            label_Islogin.Text = Usuario.IsLogin;
            label_Perfil.Text = Usuario.Perfil;
            senha = Usuario.Password;
            label_senha.Text = asterisco(senha.Length);
            label_usuario.Text = Usuario.Login;
            timer1.Start();
            
        }
        //
        //criar uma string de asterisco 
        //
        public string asterisco(int n)
        {
            return new String('*', n);
        }
        //
        //atualizar sempre as variaveis 
        //
        private void timer1_Tick(object sender, EventArgs e)
        {
            label_IPDB.Text = Usuario.IPDB;
            label_Islogin.Text = Usuario.IsLogin;
            label_Perfil.Text = Usuario.Perfil;
            senha = Usuario.Password;
            label_senha.Text = asterisco(senha.Length);
            label_usuario.Text = Usuario.Login;
        }
    }
}
