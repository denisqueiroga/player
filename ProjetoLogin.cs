﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;


namespace WindowsFormsApplication1
{
    public partial class ProjetoLogin : System.Windows.Forms.Form
    {
        //
        // inicializacao
        //
        public ProjetoLogin()
        {
            InitializeComponent();
            this.Perfil_Combo.SelectedIndex = 0;
            ConfigFile ConfigFile1 = new ConfigFile();
            ConfigFile1.readfile();
            Usuario.IPDB = ConfigFile1.ConfigGravado[0];

            if (Usuario.IsLogin == "1" )
            {
            this.buttonlogin.Text = "Logout";
            this.textBoxUsuario.Enabled = false;
            this.textBoxUsuario.Text = Usuario.Login;
            this.maskedTextBoxSenha.Enabled = false;
            this.Perfil_Combo.Enabled = false;
            }
            else
            {
            this.buttonlogin.Text = "Login";
            this.textBoxUsuario.Enabled = true;
            this.maskedTextBoxSenha.Enabled = true;
            this.Perfil_Combo.Enabled = true;
            }
            // Define o campo da senha com vazio
            maskedTextBoxSenha.Text = "";
            // O caracter de senha é asteristico
            maskedTextBoxSenha.PasswordChar = '*';
            toolTip1.Show("Tooltip text goes here", buttonlogin);
            toolTip1.SetToolTip(buttonlogin, "aa");
        }
        //
        // button cancelar e logar
        //
        private void buttonlogin_Click(object sender, EventArgs e)
        {
            // var global esta logado
            if (Usuario.IsLogin == "1")
            {
                //troca os botoes do form para poder logar novamente
                this.buttonlogin.Text = "Login";
                this.textBoxUsuario.Enabled = true;
                this.maskedTextBoxSenha.Enabled = true;
                this.Perfil_Combo.Enabled = true; 
                // var global esta deslogado
                Usuario.Perfil = null;
                Usuario.IsLogin = null;
                Usuario.Login = null;
                Usuario.Password = null;
            }
           else
            {

                // verifica no banco de dados
                DBConnectLogin DBConnectLogin1 = new DBConnectLogin(textBoxUsuario.Text, maskedTextBoxSenha.Text,Perfil_Combo.Text);
                if (DBConnectLogin1.UserConect)
                {
                    // troca o formulario para efeturar o logout

                    this.buttonlogin.Text = "Logout";
                    this.textBoxUsuario.Enabled = false;
                    this.maskedTextBoxSenha.Enabled = false;
                    this.Perfil_Combo.Enabled = false;
                    // var global esta conectado
                    Usuario.Perfil = Perfil_Combo.SelectedItem.ToString();
                    Usuario.IsLogin = "1";
                    Usuario.Login = textBoxUsuario.Text;
                    Usuario.Password = maskedTextBoxSenha.Text;
                    // fechar a janela
                    FecharFormulariosFilhos();
                }
                else
                {
                    MessageBox.Show("Usuario ou senha invalida");
                }
            }
        }
        private void buttoncancelar_Click(object sender, EventArgs e)
        {
            FecharFormulariosFilhos();
        }
        // 
        // Enter texte box - igual ao clicar no bottao login
        //
        private void maskedTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonlogin_Click(this, new EventArgs());
            }
        }
        // 
        // encerrar todas os subforms
        //
        private void FecharFormulariosFilhos()
        {
            // percorre todos os formulários abertos
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                // se o formulário for filho
                if (Application.OpenForms[i].IsMdiChild)
                {
                    // fecha o formulário
                    Application.OpenForms[i].Close();
                }
            }
        }
    }
}


