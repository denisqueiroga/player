﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;

namespace WindowsFormsApplication1
{
    public partial class ConfiguracaoCadastroUsuario : System.Windows.Forms.Form
    {
        //
        // iniciacao
        //
        public ConfiguracaoCadastroUsuario()
        {
            InitializeComponent();


            // Set to no text.
            textboxSenha.Text = "";
            ApagarSenha.Text = "";
            // The password character is an asterisk.
            textboxSenha.PasswordChar = '*';
            ApagarSenha.PasswordChar = '*';
            // The control will allow no more than 14 characters.
            textboxSenha.MaxLength = 14;

            //bloquear o campo trocarnome
            TrocarNome.Enabled = false;
            TrocarPerfil.Enabled = false;
            ApagarNome.Enabled = false;
            ApagarPerfil.Enabled = false;
            ApagarSenha.Enabled = false;

            // Ler os dados no dataGridView
            dataGridView1.DataSource = Load();
        }
        //
        // dataGrid
        //
        private BindingSource Load()
        {
            //MySqlConnection sqliteCon = new MySqlConnection(dbConnectionString);
            MySqlConnection connection;
            string server;
            string database;
            string userid;
            string password;


            // server = "192.168.1.106";
            server = Usuario.IPDB;
            database = "mydb";
            userid = "player";
            password = "player";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "USERID=" + userid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
            DataSet ds = new DataSet();
            BindingSource bs = new BindingSource();

            //open connection to database
            connection.Open();

            //select statament
            string Query = "select * from user";
            MySqlCommand cmd = new MySqlCommand(Query, connection);
            //Adapter
            // DataTable dataTable = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
            adapter.SelectCommand = cmd;
            adapter.Fill(ds);
            bs.DataSource = ds.Tables[0];
            return bs;
        }
        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dataGridView1.CurrentCell.ColumnIndex == 3)//select target column
            {
                TextBox textBox = e.Control as TextBox;
                if (textBox != null)
                {
                    textBox.UseSystemPasswordChar = true;
                }
            }
        }
        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "senha" && (e.Value != null))
            {
                dataGridView1.Rows[e.RowIndex].Tag = e.Value;
                e.Value = new String('*', e.Value.ToString().Length);
            }
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //proibir edicao
            dataGridView1.Rows[e.RowIndex].ReadOnly = true;
            if (dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null)
            {
                dataGridView1.Rows[e.RowIndex].ReadOnly = false;
            }

            // ler os dados no painel

            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];

                ApagarNome.Text = row.Cells["nome"].Value.ToString();
                TrocarNome.Text = row.Cells["nome"].Value.ToString();
                ApagarPerfil.Text = row.Cells["perfil"].Value.ToString();
                ApagarSenha.Text = row.Cells["senha"].Value.ToString();
                TrocarSenha1.Text = "";
                TrocarSenha2.Text = "";
            }
        }
        //
        //apagar usuario
        //
        private void buttonApgar_Click(object sender, EventArgs e)
        {
            // verifica se a senha é igual

            DataGridViewRow row = this.dataGridView1.Rows[dataGridView1.CurrentRow.Index];

            string usuario1 = row.Cells["nome"].Value.ToString();
            string perfil1 = row.Cells["perfil"].Value.ToString();
            ApagarUsuario(usuario1, perfil1);
            dataGridView1.DataSource = Load();
           
                MessageBox.Show("usuario apagado");
            
        }
        private void ApagarUsuario(string Myusuario, string MyPerfil)
        {
            //MySqlConnection sqliteCon = new MySqlConnection(dbConnectionString);
            MySqlConnection connection;
            string server;
            string database;
            string userid;
            string password;


            // server = "192.168.1.106";
            server = Usuario.IPDB;
            database = "mydb";
            userid = "player";
            password = "player";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "USERID=" + userid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
            DataSet ds = new DataSet();
            BindingSource bs = new BindingSource();
            string Query = "DELETE FROM `user` WHERE `user`.`nome`= '" + Myusuario + "' AND `user`.`perfil` ='" + MyPerfil + "';";
            try

            {
                using (MySqlConnection cn = new MySqlConnection(connection.ConnectionString))
                {
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.Connection = cn;

                    cmd.CommandText = Query;
                    cn.Open();
                    int numRowsUpdated = cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
            }
            catch (MySqlException exSql)
            {
                Console.Error.WriteLine("Error - SafeMySql: SQL Exception: " + Query);
                Console.Error.WriteLine(exSql.StackTrace);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error - SafeMySql: Exception: " + Query);
                Console.Error.WriteLine(ex.StackTrace);
            }
        }
        //
        //atualizar senha nova
        //
        private void TrocarButton_Click_2(object sender, EventArgs e)
        {
            // verifica se a senha é igual

            if (validacaosenha.Text == "ok - senha igual")
            {
                // grava as senha
                SalvarSenha(TrocarNome.Text, TrocarSenha1.Text, TrocarPerfil.Text);
                // atualizar os dados  datagridview
                dataGridView1.DataSource = Load();
            }
            else
            {
                MessageBox.Show(validacaosenha.Text);
            }
        }
        private void SalvarSenha(string Myusuario, string Mysenha, string MyPerfil)
        {
            //MySqlConnection sqliteCon = new MySqlConnection(dbConnectionString);
            MySqlConnection connection;
            string server;
            string database;
            string userid;
            string password;


            // server = "192.168.1.106";
            server = Usuario.IPDB;
            database = "mydb";
            userid = "player";
            password = "player";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "USERID=" + userid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
            DataSet ds = new DataSet();
            BindingSource bs = new BindingSource();

            //open connection to database
            // connection.Open();

            //select statament
            //string  Mysenha = "aa";
            //string Myusuario = "denis";
            //string MyPerfil = "Administrador"; 
            // string Query = "UPDATE `user` SET `senha`='" + Mysenha + "' WHERE nome='"  + Myusuario +
            //               "'  and `perfil`='" + MyPerfil + "';";
            // string Query = "UPDATE `user` SET `senha`='we' WHERE nome='denis'  and `perfil`='Operador'";
            // MySqlCommand cmd = new MySqlCommand(Query, connection);

            string Query = "UPDATE `user` SET `senha`='" + Mysenha + "' WHERE nome='" + Myusuario +
                           "'  and `perfil`='" + MyPerfil + "';";
            try

            {
                using (MySqlConnection cn = new MySqlConnection(connection.ConnectionString))
                {
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.Connection = cn;

                    cmd.CommandText = Query;
                    cn.Open();
                    int numRowsUpdated = cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
            }
            catch (MySqlException exSql)
            {
                Console.Error.WriteLine("Error - SafeMySql: SQL Exception: " + Query);
                Console.Error.WriteLine(exSql.StackTrace);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error - SafeMySql: Exception: " + Query);
                Console.Error.WriteLine(ex.StackTrace);
            }
        }
        //
        // novo usuario
        //
        private void NovoUsuario (string Myusuario, string Mysenha, string MyPerfil)
        {
            MySqlConnection connection;
            string server;
            string database;
            string userid;
            string password;

            server = Usuario.IPDB;
            database = "mydb";
            userid = "player";
            password = "player";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "USERID=" + userid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
            DataSet ds = new DataSet();
            BindingSource bs = new BindingSource();

            string Query = "INSERT INTO `user` (`id`, `nome`, `senha`, `perfil`) VALUES(NULL,'" + Myusuario + "', '" + Mysenha + "', '" + MyPerfil + "');";
            try

            {
                using (MySqlConnection cn = new MySqlConnection(connection.ConnectionString))
                {
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.Connection = cn;

                    cmd.CommandText = Query;
                    cn.Open();
                    int numRowsUpdated = cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    MessageBox.Show("Usuario Incluido");
                }
            }
            catch (MySqlException exSql)
            {
                Console.Error.WriteLine("Error - SafeMySql: SQL Exception: " + Query);
                Console.Error.WriteLine(exSql.StackTrace);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error - SafeMySql: Exception: " + Query);
                Console.Error.WriteLine(ex.StackTrace);
            }
        }
        private void buttonSalvar_Click(object sender, EventArgs e)
        {
            if ((textboxNome.Text != "Nome") && (textboxSenha.Text != ""))
            {
                   NovoUsuario(textboxNome.Text, textboxSenha.Text, comboBoxGrupo.Text);
                    // atualizar os dados  datagridview
                    dataGridView1.DataSource = Load();      
            }
            else
            {
                MessageBox.Show("Senha em branco");
            }

        }
        //
        //validacao da senha
        //
        private string senhaigual(string senha1, string senha2)
        {

            if (senha1 == "" ^ senha2 == "")
            {
                return "erro - uma senha vazia";
            }

            if (senha1 == senha2) 
            {
                return "ok - senha igual";
            }
            if (senha1 != senha2)
            {
                return "erro - senha diferentes";
            }

            return "padrao";
        }
        private void TrocarSenha1_TextChanged(object sender, EventArgs e)
        {
            validacaosenha.Text = senhaigual(TrocarSenha1.Text, TrocarSenha2.Text);
        }
        private void TrocarSenha2_TextChanged(object sender, EventArgs e)
        {
            validacaosenha.Text = senhaigual(TrocarSenha1.Text, TrocarSenha2.Text);
        }
        //
        // limpa o formulario
        //
        private void buttonLimpar_Click(object sender, EventArgs e)
        {
            textboxSenha.Text = "";
            textboxNome.Text = "";
        }
        private void textboxNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (textboxNome.Text == "Nome")
            {
                textboxNome.Text = " ";
            }
        }
    }
}
