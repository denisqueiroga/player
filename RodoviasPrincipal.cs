﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;





namespace WindowsFormsApplication1
{
 

    public partial class RodoviasPrincipal : System.Windows.Forms.Form
    {
        public string user;
        public Boolean UserLoged;
        public Boolean ConfigExist = false;
        public RodoviasPrincipal()
        {
            
            InitializeComponent();
            ExiteConfigFile();
            Menu();

            //string curDir = System.IO.Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory.ToString());
            //string ConfigFile = curDir + "config.txt";
            //MessageBox.Show = File.ReadLines(configPath).First();

        }

        //verifica se existe o arquivo config.txt e grava na variavel global Usuario.IPDB o IP
        private void ExiteConfigFile()
        {
            ConfigFile ConfigFile1 = new ConfigFile();
            ConfigExist = ConfigFile1.ConfigFileTrue;
            if (ConfigFile1.ConfigFileTrue)
            {
                ConfigFile1.readfile();
                Usuario.IPDB = ConfigFile1.ConfigGravado[0];
            }
            else
            {
                MessageBox.Show("Configurar o IP do servidor BD. Em Configurações> Configurar");
            }
        }
 
        // os menus de acesso ao programa sem acesso quando não esta logado
        private void Menu()
        {
            if (Usuario.IsLogin != "1")
            {
                this.abrirProjetoToolStripMenuItem.Enabled = false;

                if (Usuario.Perfil == "Administrador")
                {
                    this.cadastroDeProjetosToolStripMenuItem.Enabled = true;
                    this.cadastroDeUsuáriosToolStripMenuItem.Enabled = true;
                    this.exportarToolStripMenuItem.Enabled = true;
                }
                else
                {
                    this.cadastroDeProjetosToolStripMenuItem.Enabled = false;
                    this.cadastroDeUsuáriosToolStripMenuItem.Enabled = false;
                    this.exportarToolStripMenuItem.Enabled = false;

                }


            }
            else
            {
                this.abrirProjetoToolStripMenuItem.Enabled = true;

                if (Usuario.Perfil == "Administrador")
                {
                    this.cadastroDeProjetosToolStripMenuItem.Enabled = true;
                    this.cadastroDeUsuáriosToolStripMenuItem.Enabled = true;
                    this.exportarToolStripMenuItem.Enabled = true;
                }
                else
                {
                    this.cadastroDeProjetosToolStripMenuItem.Enabled = false;
                    this.cadastroDeUsuáriosToolStripMenuItem.Enabled = false;
                    this.exportarToolStripMenuItem.Enabled = false;

                }

            }


        }  


        private void Principal_Load(object sender, EventArgs e)
        {

        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void configurarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConfiguracaoConfigurar newMDIChild = new ConfiguracaoConfigurar();
            // Set the Parent Form of the Child window.
            newMDIChild.MdiParent = this;
            // Display the new form.
            newMDIChild.Show();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ProjetoLogin newMDIChild = new ProjetoLogin();
            // Set the Parent Form of the Child window.
            newMDIChild.MdiParent = this;
            // Display the new form.
            newMDIChild.Show();
        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Status newMDIChild = new Status();
            // Set the Parent Form of the Child window.
            newMDIChild.MdiParent = this;
            // Display the new form.
            newMDIChild.Show();
        }

        private void abrirProjetoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            ProjetoAbrirProjeto newMDIChild = new ProjetoAbrirProjeto();
            // Set the Parent Form of the Child window.
            newMDIChild.MdiParent = this;
            // Display the new form.
            newMDIChild.Show();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
           Menu();
        }

        private void configuraçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
           Menu();
        }

        private void Rodovias_MouseClick(object sender, MouseEventArgs e)
        {
          //  Menu();
        }

        private void cadastroDeUsuáriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConfiguracaoCadastroUsuario newMDIChild = new ConfiguracaoCadastroUsuario();
            // Set the Parent Form of the Child window.
            newMDIChild.MdiParent = this;
            // Display the new form.
            newMDIChild.Show();
        }

        private void RodoviasPrincipal_Leave(object sender, EventArgs e)
        {
    
        }
        private void FecharFormulariosFilhos()
        {
            // percorre todos os formulários abertos
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                // se o formulário for filho
                if (Application.OpenForms[i].IsMdiChild)
                {
                    // fecha o formulário
                    Application.OpenForms[i].Close();
                }
            }
        }


        private void exportarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void projetosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cadastroDeProjetosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FecharFormulariosFilhos();
            ConfiguracaoCadastroProjetos newMDIChild = new ConfiguracaoCadastroProjetos();
            // Set the Parent Form of the Child window.
            newMDIChild.MdiParent = this;
            // Display the new form.
            newMDIChild.Show();
        }

        private void toolStripMenuItem2_Click_1(object sender, EventArgs e)
        {

        }

        private void exportarimportarCadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FecharFormulariosFilhos();
            ProjetoExportarImportar newMDIChild = new ProjetoExportarImportar();
            // Set the Parent Form of the Child window.
            newMDIChild.MdiParent = this;
            // Display the new form.
            newMDIChild.Show();
        }

        private void exportarEventosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FecharFormulariosFilhos();
            ProjetoExportar newMDIChild = new ProjetoExportar();
            // Set the Parent Form of the Child window.
            newMDIChild.MdiParent = this;
            // Display the new form.
            newMDIChild.Show();
        }
    }
}
