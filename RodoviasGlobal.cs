﻿using System;
namespace WindowsFormsApplication1
{
    public static class Usuario
    {
        private static string m_perfil = "";
        public static string Perfil
        {
            get { return m_perfil; }
            set { m_perfil = value; }
        }
        private static string _login = "";
        public static string Login
        {
            get { return _login; }
            set { _login = value; }
        }
        private static string _password = "";
        public static string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        private static string _IsLogin = "";
        public static string IsLogin
        {
            get { return _IsLogin; }
            set { _IsLogin = value; }
        }
        private static string _IPDB = "";
        public static string IPDB
        {
            get { return _IPDB; }
            set { _IPDB = value; }
        }
        private static string _ImagePath = "";
        public static string ImagePath
        {
            get { return _ImagePath; }
            set { _ImagePath = value; }
        }
        private static int _IMAGENS_VIDEO1 = 0;
        public static int IMAGENS_VIDEO1
        {
            get { return _IMAGENS_VIDEO1; }
            set { _IMAGENS_VIDEO1 = value; }
        }
        private static int _IMAGENS_VIDEO2 = 0;
        public static int IMAGENS_VIDEO2
        {
            get { return _IMAGENS_VIDEO2; }
            set { _IMAGENS_VIDEO2 = value; }
        }
    }
}
