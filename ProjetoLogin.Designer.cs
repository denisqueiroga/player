﻿namespace WindowsFormsApplication1
{
    partial class ProjetoLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonlogin = new System.Windows.Forms.Button();
            this.labelUsuario = new System.Windows.Forms.Label();
            this.maskedTextBoxSenha = new System.Windows.Forms.MaskedTextBox();
            this.textBoxUsuario = new System.Windows.Forms.TextBox();
            this.labelPerfil = new System.Windows.Forms.Label();
            this.Perfil_Combo = new System.Windows.Forms.ComboBox();
            this.buttoncancelar = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.labelSenha = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonlogin
            // 
            this.buttonlogin.Location = new System.Drawing.Point(170, 164);
            this.buttonlogin.Name = "buttonlogin";
            this.buttonlogin.Size = new System.Drawing.Size(75, 23);
            this.buttonlogin.TabIndex = 9;
            this.buttonlogin.Text = "&Login";
            this.toolTip1.SetToolTip(this.buttonlogin, "teste");
            this.buttonlogin.UseVisualStyleBackColor = true;
            this.buttonlogin.Click += new System.EventHandler(this.buttonlogin_Click);
            // 
            // labelUsuario
            // 
            this.labelUsuario.AutoSize = true;
            this.labelUsuario.Location = new System.Drawing.Point(50, 47);
            this.labelUsuario.Name = "labelUsuario";
            this.labelUsuario.Size = new System.Drawing.Size(43, 13);
            this.labelUsuario.TabIndex = 1;
            this.labelUsuario.Text = "Usuário";
            // 
            // maskedTextBoxSenha
            // 
            this.maskedTextBoxSenha.Location = new System.Drawing.Point(170, 82);
            this.maskedTextBoxSenha.Name = "maskedTextBoxSenha";
            this.maskedTextBoxSenha.Size = new System.Drawing.Size(100, 20);
            this.maskedTextBoxSenha.TabIndex = 4;
            this.maskedTextBoxSenha.KeyDown += new System.Windows.Forms.KeyEventHandler(this.maskedTextBox1_KeyDown);
            // 
            // textBoxUsuario
            // 
            this.textBoxUsuario.Location = new System.Drawing.Point(170, 40);
            this.textBoxUsuario.Name = "textBoxUsuario";
            this.textBoxUsuario.Size = new System.Drawing.Size(100, 20);
            this.textBoxUsuario.TabIndex = 3;
            // 
            // labelPerfil
            // 
            this.labelPerfil.AutoSize = true;
            this.labelPerfil.Location = new System.Drawing.Point(50, 130);
            this.labelPerfil.Name = "labelPerfil";
            this.labelPerfil.Size = new System.Drawing.Size(30, 13);
            this.labelPerfil.TabIndex = 6;
            this.labelPerfil.Text = "Perfil";
            // 
            // Perfil_Combo
            // 
            this.Perfil_Combo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.Perfil_Combo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Perfil_Combo.FormattingEnabled = true;
            this.Perfil_Combo.Items.AddRange(new object[] {
            "Operador",
            "Administrador"});
            this.Perfil_Combo.Location = new System.Drawing.Point(170, 127);
            this.Perfil_Combo.Name = "Perfil_Combo";
            this.Perfil_Combo.Size = new System.Drawing.Size(100, 21);
            this.Perfil_Combo.TabIndex = 5;
            // 
            // buttoncancelar
            // 
            this.buttoncancelar.Location = new System.Drawing.Point(53, 164);
            this.buttoncancelar.Name = "buttoncancelar";
            this.buttoncancelar.Size = new System.Drawing.Size(75, 23);
            this.buttoncancelar.TabIndex = 8;
            this.buttoncancelar.Text = "&Cancelar";
            this.buttoncancelar.UseVisualStyleBackColor = true;
            this.buttoncancelar.Click += new System.EventHandler(this.buttoncancelar_Click);
            // 
            // labelSenha
            // 
            this.labelSenha.AutoSize = true;
            this.labelSenha.Location = new System.Drawing.Point(50, 89);
            this.labelSenha.Name = "labelSenha";
            this.labelSenha.Size = new System.Drawing.Size(38, 13);
            this.labelSenha.TabIndex = 9;
            this.labelSenha.Text = "Senha";
            // 
            // ProjetoLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 199);
            this.Controls.Add(this.labelSenha);
            this.Controls.Add(this.buttoncancelar);
            this.Controls.Add(this.Perfil_Combo);
            this.Controls.Add(this.labelPerfil);
            this.Controls.Add(this.textBoxUsuario);
            this.Controls.Add(this.maskedTextBoxSenha);
            this.Controls.Add(this.labelUsuario);
            this.Controls.Add(this.buttonlogin);
            this.Location = new System.Drawing.Point(100, 100);
            this.MaximumSize = new System.Drawing.Size(334, 238);
            this.MinimumSize = new System.Drawing.Size(334, 238);
            this.Name = "ProjetoLogin";
            this.Text = "Login";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonlogin;
        private System.Windows.Forms.Label labelUsuario;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxSenha;
        private System.Windows.Forms.TextBox textBoxUsuario;
        private System.Windows.Forms.Label labelPerfil;
        private System.Windows.Forms.ComboBox Perfil_Combo;
        private System.Windows.Forms.Button buttoncancelar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label labelSenha;
    }
}