﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;

namespace WindowsFormsApplication1
{
    public partial class ConfiguracaoConfigurar : System.Windows.Forms.Form
    {
        // milha class de arquivo txt IPservidor
        ConfigFile ConfigFile1 = new ConfigFile();
        public ConfiguracaoConfigurar()
        {
            InitializeComponent();
            ConfigFile1.readfile();

            if (ConfigFile1.ConfigGravado != null)
            {

                maskedTextBox1.Text = ConfigFile1.ConfigGravado[0];
                textBox2.Text = ConfigFile1.ConfigGravado[1];
            }
        }
        private void buttonSalvar_Click(object sender, EventArgs e)
        {
           if ( CheckIPValid(maskedTextBox1.Text))
             {
                          
               string[] conf = new string[2] { maskedTextBox1.Text, textBox2.Text };
               ConfigFile1.writefile(conf);
               Usuario.IPDB = ConfigFile1.ConfigGravado[0];
               FecharFormulariosFilhos();
            }
           else
            {
                MessageBox.Show("Não é valido o ip" + maskedTextBox1.Text);
            }
        }
        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            FecharFormulariosFilhos();
        }
        private void buttonDialogBox_Click(object sender, EventArgs e)
        {
            //
            // This event handler was created by double-clicking the window in the designer.
            // It runs on the program's startup routine.
            //
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                //
                // The user selected a folder and pressed the OK button.

                
                textBox2.Text = folderBrowserDialog1.SelectedPath;
                
            }
        }
        //
        //  Fecha formularios internos
        //
        private void FecharFormulariosFilhos()
        {
            // percorre todos os formulários abertos
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                // se o formulário for filho
                if (Application.OpenForms[i].IsMdiChild)
                {
                    // fecha o formulário
                    Application.OpenForms[i].Close();
                }
            }
        }
        //
        //  verifica o texto do ip é valido
        //
        public static bool CheckIPValid(string strIP)
        {
            // string pattern = @"\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b";
            //  string ip1 = strIP;
            // if  Regex.IsMatch(ip1, pattern){


            IPAddress result = null;
            return
          !String.IsNullOrEmpty(strIP) &&
          IPAddress.TryParse(strIP, out result);
            // }
        }
    }
}
