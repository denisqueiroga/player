﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Windows;
using System.Globalization;
using WMPLib;
using MySql.Data.MySqlClient;
using System.Data;

namespace WindowsFormsApplication1
{

    public partial class ProjetoEventos : System.Windows.Forms.Form
    {
        Boolean backforward = false;
        string KeyChar1;
        // variaveis de controle do eventos para ser incluidas no DB
        // variavel de evento com conecxao de banco de dados.
        DBEvento DBEvento1 = new DBEvento();
        DBButton DBButton1 = new DBButton();


        string Keydata1;
        // veiculos
        int painelVeiculos = 1 ;
        string[] NomeBotaoVeiculos = new string[] { "veiculo 1", "veiculo 2", "veiculo 3" };
        string[] AtalhoBotaoVeiculos = new string[] { "A", "B", "C" };
        int[] PontoXVeiculos = new int[] { 0, 1, 0 };
        int[] PontoYVeiculos = new int[] { 0, 0, 1 };
        // onibus
        int painelOnibus = 2;
        int[] PontoXOnibus = new int[] { 0, 1, 0, 1 };
        int[] PontoYOnibus = new int[] { 0, 0, 1, 1 };
        string[] NomeBotaoOnibus = new string[] { "onibus 1", "onibus 2", "onibus 3", "onibus 4" };
        string[] AtalhoBotaoOnibus = new string[] { "I", "J", "K", "L" };
        // caminhao
        int painelCaminhao = 3;
        int[] PontoXCaminhao = new int[] { 0, 1, 0, 1, 0 };
        int[] PontoYCaminhao = new int[] { 0, 0, 1, 1, 2 };
        string[] NomeBotaoCaminhao = new string[] { "caminhao 1", "caminhao 2", "caminhao 3", "caminhao 4", "caminhao 5" };
        string[] AtalhoBotaoCaminhao = new string[] { "D", "E", "F", "G", "H" };
        // Eixo
        int painelEixo = 4;
        int[] PontoXEixo = new int[] { 0, 1, 0, 1, 0 };
        int[] PontoYEixo = new int[] { 0, 0, 1, 1, 2 };
        string[] NomeBotaoEixo = new string[] { "eixo 1", "eixo 2", "eixo 3", "eixo 4", "eixo 5" };
        string[] AtalhoBotaoEixo = new string[] { "M", "N", "O", "P", "R" };
        //
        // Iniciacao do objeto
        //
        public ProjetoEventos(int cadastrogrupo_id)
        {
            InitializeComponent();
            //
            // aumenta o tamanho do painel 
            //
            this.Width = Screen.PrimaryScreen.WorkingArea.Width;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;

            //
            //  grava na variavel DBeventos os projeto_id e o projeto_name e o ponto
            // atravez do id da subtable descobre o id a tabela pai
            DBEvento1.cadastro_grupo_id = cadastrogrupo_id;
            DBEvento1.EventoProjeto_id(cadastrogrupo_id);
            DBEvento1.EventoPonto(cadastrogrupo_id);
            DBEvento1.EventoProjetoNome();
            DBEvento1.EventoCadastroVideo();
            DBEvento1.EventoUsuario();



            //
            //Ler dados da classificacao
            //
            DBButton1.DBButton_ler_dados(DBEvento1.projeto_id);

            //
            //criar os botaoes baseado em um parametro de classificacao
            //
            criar_botão();
            //
            // ler a tabela de dados
            //
            dataGridView1.DataSource = Load();




            //
            // inicio_video;
            //
            if ((DBEvento1.cadastro_video_arquivo1 != null) && (DBEvento1.cadastro_video_arquivo2 != null))
            {
                string file1, file2;
                file1 = DBEvento1.cadastro_video_path1 + "\\" + DBEvento1.cadastro_video_arquivo1;
                file2 = DBEvento1.cadastro_video_path2 + "\\" + DBEvento1.cadastro_video_arquivo2;

                this.axWindowsMediaPlayer1.URL = file1;
                this.axWindowsMediaPlayer1.Ctlcontrols.play();
                this.axWindowsMediaPlayer2.URL = file2;
                this.axWindowsMediaPlayer2.Ctlcontrols.play();

            }
            if ((DBEvento1.cadastro_video_arquivo1 != null) && (DBEvento1.cadastro_video_arquivo2 == null))
            {

                // layout exibir somento o video A

                //ocultar a tabela de video item 2
                this.tableLayoutPanelVideo.ColumnStyles[0].SizeType = SizeType.Percent;
                this.tableLayoutPanelVideo.ColumnStyles[0].Width = 100;

                this.tableLayoutPanelVideo.ColumnStyles[1].SizeType = SizeType.Percent;
                this.tableLayoutPanelVideo.ColumnStyles[1].Width = 0;

                // tronar o player 2 vissivel    
                axWindowsMediaPlayer1.Ctlcontrols.pause();
                axWindowsMediaPlayer2.Ctlcontrols.pause();
                axWindowsMediaPlayer1.Visible = true;
                axWindowsMediaPlayer2.Visible = false;

                // toca o video
                string file1, file2;
                file1 = DBEvento1.cadastro_video_path1 + "\\" + DBEvento1.cadastro_video_arquivo1;
                //file2 = DBEvento1.cadastro_video_path2 + "\\" + DBEvento1.cadastro_video_arquivo2;

                this.axWindowsMediaPlayer1.URL = file1;
                this.axWindowsMediaPlayer1.Ctlcontrols.play();
                //this.axWindowsMediaPlayer2.URL = file2;
                //this.axWindowsMediaPlayer2.Ctlcontrols.play();


                // inicia o timer
                timercontador.Interval = 1000;
                timercontador.Start();

            }
        }
        //
        // Controle velocidade e play e pause , etc dos Videos
        //
        void inicio_video(string a, string b)
        {

            // Open the selected file to read.

            // this.axWindowsMediaPlayer1.URL = a;
            // this.axWindowsMediaPlayer2.URL = b;
            // inicial o timer para atualizar o display do contatador de tempo
            timercontador.Interval = 1000;
            timercontador.Start();
        }
        private void buttonPause_Click(object sender, EventArgs e)
        {
            timerbackforward.Stop();
            this.axWindowsMediaPlayer1.Ctlcontrols.pause();
            this.axWindowsMediaPlayer2.Ctlcontrols.pause();

        }
        private void buttonPlay_Click(object sender, EventArgs e)
        {
            timerbackforward.Stop();
            this.axWindowsMediaPlayer1.settings.rate = 1;
            this.axWindowsMediaPlayer2.settings.rate = 1;
            this.axWindowsMediaPlayer2.Ctlcontrols.currentPosition = this.axWindowsMediaPlayer1.Ctlcontrols.currentPosition + Convert.ToDouble(numericUpDowndelay.Value);
            this.axWindowsMediaPlayer1.Ctlcontrols.play();
            this.axWindowsMediaPlayer2.Ctlcontrols.play();
        }
        private void buttonPlayForward_Click(object sender, EventArgs e)
        {
            timerbackforward.Stop();
            // para os dois videos
            this.axWindowsMediaPlayer1.Ctlcontrols.pause();
            this.axWindowsMediaPlayer2.Ctlcontrols.pause();
            this.axWindowsMediaPlayer1.settings.rate = Convert.ToDouble(numericUpDown1.Value);
            this.axWindowsMediaPlayer2.settings.rate = Convert.ToDouble(numericUpDown1.Value);
            this.axWindowsMediaPlayer1.Ctlcontrols.play();
            this.axWindowsMediaPlayer2.Ctlcontrols.play();
        }
        private void buttonPlayBackward_Click(object sender, EventArgs e)
        {
            // if (this.axWindowsMediaPlayer1.settings.isAvailable("Rate"))
            // {
            //     MessageBox("Nao possivel");
            // }

            timerbackforward.Start();



            // this.axWindowsMediaPlayer1.Ctlcontrols.fastReverse();
            // this.axWindowsMediaPlayer2.Ctlcontrols.fastReverse();
        }
        private void buttonMover_Click(object sender, EventArgs e)
        {
            timerbackforward.Stop();
            this.axWindowsMediaPlayer1.Ctlcontrols.pause();
            this.axWindowsMediaPlayer2.Ctlcontrols.pause();
            double temp = axWindowsMediaPlayer1.Ctlcontrols.currentPosition;
            double timevoltar = Convert.ToDouble(numericUpDown2.Value);
            temp = temp + timevoltar;
            this.axWindowsMediaPlayer1.Ctlcontrols.currentPosition = temp;
            this.axWindowsMediaPlayer2.Ctlcontrols.currentPosition = temp + Convert.ToDouble(numericUpDowndelay.Value);
            currentPositionStringA.Text = this.axWindowsMediaPlayer1.Ctlcontrols.currentPositionString;
        }
        private void buttonSincronizarVideo_Click(object sender, EventArgs e)
        {
            this.axWindowsMediaPlayer1.Ctlcontrols.pause();
            this.axWindowsMediaPlayer2.Ctlcontrols.pause();
            this.axWindowsMediaPlayer2.Ctlcontrols.currentPosition = this.axWindowsMediaPlayer1.Ctlcontrols.currentPosition + Convert.ToDouble(numericUpDowndelay.Value);
            //* removido  pois os dois video nao comecao juntos 
            //*   this.axWindowsMediaPlayer1.Ctlcontrols.play();
            //*   this.axWindowsMediaPlayer2.Ctlcontrols.play(); 
        }
        private void buttonVer_Click(object sender, EventArgs e)
        {






            string tempo = "";
            //   tempo = listBox1.SelectedItem.ToString();
            this.axWindowsMediaPlayer1.Ctlcontrols.currentPosition = DBEvento1.evento_tempo1;
            this.axWindowsMediaPlayer2.Ctlcontrols.currentPosition = DBEvento1.evento_tempo2;
            timercontador.Start();
        }
        private void axWindowsMediaPlayer1_PositionChange(object sender, AxWMPLib._WMPOCXEvents_PositionChangeEvent e)
        {
            // mudando a possicao do player 1 muda a possicao do player 2
            this.axWindowsMediaPlayer2.Ctlcontrols.currentPosition = this.axWindowsMediaPlayer1.Ctlcontrols.currentPosition + Convert.ToDouble(numericUpDowndelay.Value);
        }
        private void axWindowsMediaPlayer2_PositionChange(object sender, AxWMPLib._WMPOCXEvents_PositionChangeEvent e)
        {
            //* MessageBox.Show ("1");
            //*  this.axWindowsMediaPlayer1.Ctlcontrols.currentPosition = this.axWindowsMediaPlayer2.Ctlcontrols.currentPosition;

        }
        private void timercontador_Tick(object sender, EventArgs e)
        {
            // tempos do video A
            currentPositionStringA.Text = this.axWindowsMediaPlayer1.Ctlcontrols.currentPositionString;
            labelcurrentPositionA.Text = this.axWindowsMediaPlayer1.Ctlcontrols.currentPosition.ToString();
            DBEvento1.evento_tempo1 = this.axWindowsMediaPlayer1.Ctlcontrols.currentPosition;

            // tempos do video B
            currentPositionStringB.Text = this.axWindowsMediaPlayer2.Ctlcontrols.currentPositionString;
            labelcurrentPositionB.Text = this.axWindowsMediaPlayer2.Ctlcontrols.currentPosition.ToString();
            DBEvento1.evento_tempo2 = this.axWindowsMediaPlayer2.Ctlcontrols.currentPosition;

            // velocidade dos videos
            Velocidade1.Text = "Velocidade do video 1 = " + this.axWindowsMediaPlayer1.settings.rate;
            Velocidade2.Text = "Velocidade do video 2 = " + this.axWindowsMediaPlayer2.settings.rate;


        }
        private void timerbackforward_Tick(object sender, EventArgs e)
        {
            double a = axWindowsMediaPlayer1.Ctlcontrols.currentPosition;
            double b = Convert.ToDouble(numericUpDown1.Value);
            if (a >= 0.3)
            {

                axWindowsMediaPlayer1.Ctlcontrols.currentPosition = a - b / 3;
                axWindowsMediaPlayer2.Ctlcontrols.currentPosition = a - b / 3;
            }
            else
            {
                timerbackforward.Stop();

            }
        }
        //    
        //layout
        //
        private void buttonvideo2_Click(object sender, EventArgs e)
        {
            // layout exibir somente dois videos A e B

            //ocultar a tabela de video item 2
            this.tableLayoutPanelVideo.ColumnStyles[0].SizeType = SizeType.Percent;
            this.tableLayoutPanelVideo.ColumnStyles[0].Width = 50;

            this.tableLayoutPanelVideo.ColumnStyles[1].SizeType = SizeType.Percent;
            this.tableLayoutPanelVideo.ColumnStyles[1].Width = 50;
            // tronar o player 2 oculto    
            axWindowsMediaPlayer1.Ctlcontrols.pause();
            axWindowsMediaPlayer2.Ctlcontrols.pause();
            axWindowsMediaPlayer1.Visible = true;
            axWindowsMediaPlayer2.Visible = true;
        }
        private void buttonvideoA_Click(object sender, EventArgs e)
        {
            // layout exibir somento o video A

            //ocultar a tabela de video item 2
            this.tableLayoutPanelVideo.ColumnStyles[0].SizeType = SizeType.Percent;
            this.tableLayoutPanelVideo.ColumnStyles[0].Width = 100;

            this.tableLayoutPanelVideo.ColumnStyles[1].SizeType = SizeType.Percent;
            this.tableLayoutPanelVideo.ColumnStyles[1].Width = 0;

            // tronar o player 2 vissivel    
            axWindowsMediaPlayer1.Ctlcontrols.pause();
            axWindowsMediaPlayer2.Ctlcontrols.pause();
            axWindowsMediaPlayer1.Visible = true;
            axWindowsMediaPlayer2.Visible = false;

        }
        private void buttonvideoB_Click(object sender, EventArgs e)
        {
            // layout exibir somente o video B
            //ocultar a tabela de video item 2
            this.tableLayoutPanelVideo.ColumnStyles[1].SizeType = SizeType.Percent;
            this.tableLayoutPanelVideo.ColumnStyles[1].Width = 100;

            this.tableLayoutPanelVideo.ColumnStyles[0].SizeType = SizeType.Percent;
            this.tableLayoutPanelVideo.ColumnStyles[0].Width = 0;

            // tronar o player 2 vissivel    
            axWindowsMediaPlayer1.Ctlcontrols.pause();
            axWindowsMediaPlayer2.Ctlcontrols.pause();
            axWindowsMediaPlayer1.Visible = false;
            axWindowsMediaPlayer2.Visible = true;

        }
        private void dataGridView1_MouseEnter(object sender, EventArgs e)
        {
            if (checkBox_exapandir.Checked)
            {
                // layout 

                this.tableLayoutPanelGeral.ColumnStyles[0].SizeType = SizeType.Percent;
                this.tableLayoutPanelGeral.ColumnStyles[0].Width = 0;

                this.tableLayoutPanelGeral.ColumnStyles[1].SizeType = SizeType.Percent;
                this.tableLayoutPanelGeral.ColumnStyles[1].Width = 0;

                this.tableLayoutPanelGeral.ColumnStyles[2].SizeType = SizeType.Percent;
                this.tableLayoutPanelGeral.ColumnStyles[2].Width = 100;


                this.tableLayoutPanelGeral.RowStyles[0].SizeType = SizeType.Percent;
                this.tableLayoutPanelGeral.RowStyles[0].Height = 100;

                this.tableLayoutPanelGeral.RowStyles[1].SizeType = SizeType.Percent;
                this.tableLayoutPanelGeral.RowStyles[1].Height = 0;

                //     
                axWindowsMediaPlayer1.Ctlcontrols.pause();
                axWindowsMediaPlayer2.Ctlcontrols.pause();
            }
        }
        private void dataGridView1_MouseLeave(object sender, EventArgs e)
        {

            if (checkBox_exapandir.Checked)
            {
                //layout
                this.tableLayoutPanelGeral.ColumnStyles[0].SizeType = SizeType.Percent;
                this.tableLayoutPanelGeral.ColumnStyles[0].Width = 68;

                this.tableLayoutPanelGeral.ColumnStyles[1].SizeType = SizeType.Percent;
                this.tableLayoutPanelGeral.ColumnStyles[1].Width = 16;

                this.tableLayoutPanelGeral.ColumnStyles[2].SizeType = SizeType.Percent;
                this.tableLayoutPanelGeral.ColumnStyles[2].Width = 16;

                this.tableLayoutPanelGeral.RowStyles[0].SizeType = SizeType.Percent;
                this.tableLayoutPanelGeral.RowStyles[0].Height = 85;

                this.tableLayoutPanelGeral.RowStyles[1].SizeType = SizeType.Percent;
                this.tableLayoutPanelGeral.RowStyles[1].Height = 15;
            }
        }
        //
        // Criar Botao em tempo de execuçao
        //
        private void criar_botão()
        {
            // tabela para criar botão

            //veiculos          //onibus          //caminhão             //eixo
            CreatingNewButtons(DBButton1.classificacao, DBButton1.painel, DBButton1.PontoX, DBButton1.PontoY, DBButton1.Atalho);

        }
        private void CreatingNewButtons(string[] NomeBotao, int[] painel, int[] PontoX, int[] PontoY, string[] Atalho)
        {
            // array de criar botao
            int horizotal = 30;
            int vertical = 30;
            Button[] buttonArray = new Button[NomeBotao.Length];

            for (int i = 0; i < buttonArray.Length; i++)
            {
                buttonArray[i] = new Button();
                buttonArray[i].Size = new Size(90, 23);
                buttonArray[i].Location = new Point(horizotal, vertical);
                this.Controls.Add(buttonArray[i]);
                if (painel[i] == 1)  // adiciona no painel 1 veiculo
                    tableLayoutPanelVeiculo1.Controls.Add(buttonArray[i], PontoX[i], PontoY[i]);
                if (painel[i] == 2)   // adiciona no painel 2 onibus
                    tableLayoutPanelVeiculo2.Controls.Add(buttonArray[i], PontoX[i], PontoY[i]);
                if (painel[i] == 3)   // adiciona no painel 3 caminao
                    tableLayoutPanelVeiculo3.Controls.Add(buttonArray[i], PontoX[i], PontoY[i]);
                if (painel[i] == 4)   // adiciona no painel 4 do eixo
                    tableLayoutPanelEixo1.Controls.Add(buttonArray[i], PontoX[i], PontoY[i]);
                buttonArray[i].Click += new EventHandler(button_Click);
                buttonArray[i].Text = NomeBotao[i];

                // cria o tip para o atalho

                toolTip1.SetToolTip(buttonArray[i], Atalho[i]);
            }
        }
        protected void button_Click(object sender, EventArgs e)
        {
            // evento do botao
            Button button = sender as Button;
            // identify which button was clicked and perform necessary actions

            // saber qual botão voce esta executando

            if (button != null)
            {
                // MessageBox.Show(button.Text);

                // tronar o player 2 vissivel    
                axWindowsMediaPlayer1.Ctlcontrols.pause();
                axWindowsMediaPlayer2.Ctlcontrols.pause();

                // define o veiculo
                if (button.Parent.Parent.Name == "tableLayoutPanelClassificacao1")
                {
                    labelVeiculoCampo.Text = button.Text;
                    DBEvento1.tipos_de_veiculos_nome = button.Text;
                }
                // define eixos no solo
                if (button.Parent.Parent.Name == "tableLayoutPanelClassificacao2")
                {
                    labelEixoCampo.Text = button.Text;
                    DBEvento1.eixo_no_solo_eixo_no_solo = button.Text;
                }

                //MessageBox.Show(button.Parent.Parent.Name);
            }



        }
        //
        // Tecla de atalho no form
        //
        private void ProjetoEventos_KeyDown(object sender, KeyEventArgs e)
        {

            Keydata1 = e.KeyData.ToString();
            int i;
            i = 0;
            foreach (string Mystring in DBButton1.Atalho)
            {
                if (Mystring == Keydata1)
                    Classificacao_keyDown(DBButton1.painel[i], DBButton1.classificacao[i]);
                i++;
            }
        }
        private void Classificacao_keyDown(int tabela, string buttontext)
        {
            switch (tabela)
            {
                case 1:
                    foreach (Control c in this.tableLayoutPanelVeiculo1.Controls)
                    {
                        if ((c is Button) && (c.Text == buttontext))
                        {
                            button_Click(c as Button, null);
                        }
                    }
                    break;
                case 2:
                    foreach (Control c in this.tableLayoutPanelVeiculo2.Controls)
                    {
                        if ((c is Button) && (c.Text == buttontext))
                        {
                            button_Click(c as Button, null);
                        }
                    }
                    break;
                case 3:
                    foreach (Control c in this.tableLayoutPanelVeiculo3.Controls)
                    {
                        if ((c is Button) && (c.Text == buttontext))
                        {
                            button_Click(c as Button, null);
                        }
                    }
                    break;
                case 4:
                    foreach (Control c in this.tableLayoutPanelEixo1.Controls)
                    {
                        if ((c is Button) && (c.Text == buttontext))
                        {
                            button_Click(c as Button, null);
                        }
                    }
                    break;
            }
        }
        //
        //Funçoes de banco de dados
        //
        private BindingSource Load()
        {
            //MySqlConnection sqliteCon = new MySqlConnection(dbConnectionString);
            MySqlConnection connection;
            string server;
            string database;
            string userid;
            string password;


            // server = "192.168.1.106";
            server = Usuario.IPDB;
            database = "mydb";
            userid = "player";
            password = "player";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "USERID=" + userid + ";" + "PASSWORD=" + password + ";" + " convert zero datetime=True;";

            connection = new MySqlConnection(connectionString);
            DataSet ds = new DataSet();
            BindingSource bs = new BindingSource();

            //open connection to database
            connection.Open();

            //select statament
            string Query = "select * from eventos where cadastro_grupo_id= '" + DBEvento1.cadastro_grupo_id + "' ORDER BY `eventos`.`evento_tempo1` DESC ";
            MySqlCommand cmd = new MySqlCommand(Query, connection);
            //Adapter
            // DataTable dataTable = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
            adapter.SelectCommand = cmd;
            adapter.Fill(ds);
            bs.DataSource = ds.Tables[0];
            return bs;
        }
        private void buttonApagar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(labeleventoid.Text)==false)
            {

                DialogResult dr = MessageBox.Show("Voce quer apagar o id=" + DBEvento1.evento_id + " ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == DialogResult.No)
                {
                    MessageBox.Show("Cancelado");

                }
                else if (dr == DialogResult.Yes)
                {
                    MessageBox.Show(" Yes");
                    DBEvento1.EventoApagar(Convert.ToInt32(labeleventoid.Text));


                    //  atualiza o dadagrid
                    dataGridView1.DataSource = Load();
                    dataGridView1.Update();
                    dataGridView1.Refresh();

                    // atualiza o label 
                    labeleventoid.Text = null;
                }

            }
            else
            {
                MessageBox.Show("Selecionar um evento na tabela");
            }
        }
        private void buttonEvento_Click(object sender, EventArgs e)
        {
            if (DBEvento1.evento_tempo1 != 0)
            //if ((DBEvento1.evento_tempo1 != 0) && (DBEvento1.evento_tempo2 != 0))
            {
                if ((string.IsNullOrEmpty(DBEvento1.tipos_de_veiculos_nome) == false) && (string.IsNullOrEmpty(DBEvento1.eixo_no_solo_eixo_no_solo) == false))
                {


                    //DBEvento1.Grid();
                    DBEvento1.EventoInsert();


                    //  atualiza o dadagrid
                    dataGridView1.DataSource = Load();
                    dataGridView1.Update();
                    dataGridView1.Refresh();

                    // limpa a classificao atual
                    DBEvento1.tipos_de_veiculos_nome = "";
                    DBEvento1.eixo_no_solo_eixo_no_solo = "";
                    labelVeiculoCampo.Text = "";
                    labelEixoCampo.Text = "";


                }
                else
                {
                    MessageBox.Show("Preencher as classificações");
                }
            }
            else
            {
                MessageBox.Show("Tempo dos dois videos igual a zero");
            }

        }
        private void buttonGravar_Click(object sender, EventArgs e)
        {
            foreach (TextBox tb in tableLayoutPanel2.Controls.OfType<TextBox>())
            {
                MessageBox.Show(tb.Text);
            }
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex >= 0) && (e.ColumnIndex >= 0))   // exclui cabecario de coluna e linha
            {
                axWindowsMediaPlayer1.Ctlcontrols.pause();
                axWindowsMediaPlayer2.Ctlcontrols.pause();
                timercontador.Stop();
                //proibir edicao
                dataGridView1.Rows[e.RowIndex].ReadOnly = true;
                if (dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null)
                {
                    dataGridView1.Rows[e.RowIndex].ReadOnly = false;
                }
                // ler os dados do GridEvento

                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                    DBEvento1.evento_tempo1 = Convert.ToDouble(row.Cells["evento_tempo1"].Value);
                    DBEvento1.evento_tempo2 = Convert.ToDouble(row.Cells["evento_tempo2"].Value);
                    DBEvento1.evento_id = Convert.ToInt32(row.Cells["id"].Value);
                    labeleventoid.Text = DBEvento1.evento_id.ToString();

                }


            }
        }
        //
        // Gravar arquivo txt
        //
        void writefile(int i)
        {
            string path = @"c:\temp\MyTest.txt";

            // This text is added only once to the file.
            if (!File.Exists(path))
            {
                // Create a file to write to.
                string createText = "Hello and Welcome" + Environment.NewLine;
                File.WriteAllText(path, createText);
            }

            // This text is always added, making the file longer over time
            // if it is not deleted.
            string appendText = "This is extra text" + Environment.NewLine;
            File.AppendAllText(path, appendText);

            // Open the file to read from.
            string readText = File.ReadAllText(path);
            Console.WriteLine(readText);
        }
        public static string GetSafeFilename(string arbitraryString)
        {
            var invalidChars = System.IO.Path.GetInvalidFileNameChars();
            var replaceIndex = arbitraryString.IndexOfAny(invalidChars, 0);
            if (replaceIndex == -1) return arbitraryString;

            var r = new StringBuilder();
            var i = 0;

            do
            {
                r.Append(arbitraryString, i, replaceIndex - i);

                switch (arbitraryString[replaceIndex])
                {
                    case '"':
                        r.Append("''");
                        break;
                    case '<':
                        r.Append('\u02c2'); // '˂' (modifier letter left arrowhead)
                        break;
                    case '>':
                        r.Append('\u02c3'); // '˃' (modifier letter right arrowhead)
                        break;
                    case '|':
                        r.Append('\u2223'); // '∣' (divides)
                        break;
                    case ':':
                        r.Append('-');
                        break;
                    case '*':
                        r.Append('\u2217'); // '∗' (asterisk operator)
                        break;
                    case '\\':
                    case '/':
                        r.Append('\u2044'); // '⁄' (fraction slash)
                        break;
                    case '\0':
                    case '\f':
                    case '?':
                        break;
                    case '\t':
                    case '\n':
                    case '\r':
                    case '\v':
                        r.Append(' ');
                        break;
                    default:
                        r.Append('_');
                        break;
                }

                i = replaceIndex + 1;
                replaceIndex = arbitraryString.IndexOfAny(invalidChars, i);
            } while (replaceIndex != -1);

            r.Append(arbitraryString, i, arbitraryString.Length - i);

            return r.ToString();
        }
        //
        // publicar trabalho
        //
        private void buttonPublicar_Click(object sender, EventArgs e)
        {
            //CriarFormulariosFilhos();
            ProjetoEventoPublicar newMDIChild = new ProjetoEventoPublicar(DBEvento1.cadastro_grupo_id);

            // Set the Parent Form of the Child window.
            newMDIChild.MdiParent = this.ParentForm;
            // Display the new form.

            newMDIChild.Show();
            timercontador.Stop();

        }
    }

}
 



