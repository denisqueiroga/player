﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;


namespace WindowsFormsApplication1
{

    //
    // class original de DB
    //
    public class DBConnect
    {
        // variaveis da coneccao do DB.
        public MySqlConnection connection = null;
        // coneccao DB
        public string connectionString;

        private string server;
        public string IPDB;
        private string database;
        private string userid;
        private string password;
        // variaveis do usurio - operador.
        private int id;
        private string usrlogin;


        //Constructor
        public DBConnect()
        {
            Initialize();
        }
        private void Initialize()
        {
            connectionString = ConnectionString();
        }
        //
        //open and close , string de coneccao ao database
        //
        private string ConnectionString()
        {
            // inicializando valores";
            server = Usuario.IPDB;
            database = "mydb";
            userid = "player";
            password = "player";
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "USERID=" + userid + ";" + "PASSWORD=" + password + ";";
            return connectionString;
        }
        public bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }
        public bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        //
        //Insert, Delete , etc
        //
        public void Insert()
        {
            string query = "INSERT INTO teste (name, age) VALUES('John Smith', '33')";

            //open connection




            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }
        public int LerUsuario()
        {
            string query = "SELECT Count(*) FROM teste";
            int Count = -1;

            //Open Connection
            if (this.OpenConnection() == true)
            {
                //Create Mysql Command
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //ExecuteScalar will return one value
                Count = int.Parse(cmd.ExecuteScalar() + "");

                //close Connection
                this.CloseConnection();

                return Count;
            }
            else
            {
                return Count;
            }
        }
        public void Update()
        {
        }
        public void Delete()
        {
        }
        public int Count()
        {
            string query = "SELECT Count(*) FROM teste";
            int Count = -1;

            //Open Connection
            if (this.OpenConnection() == true)
            {
                //Create Mysql Command
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //ExecuteScalar will return one value
                Count = int.Parse(cmd.ExecuteScalar() + "");

                //close Connection
                this.CloseConnection();

                return Count;
            }
            else
            {
                return Count;
            }

        }
        //
        //Backup , Restore
        //
        public void Backup()
        {
        }
        public void Restore()
        {
        }
        // 
        // tabelas
        //
        public void Grid()
        {
            MySqlDataReader rdr = null;

            try
            {
                connection = new MySqlConnection(connectionString);
                if (OpenConnection() == true)
                {

                    string stm = "SELECT * FROM user";
                    MySqlCommand cmd = new MySqlCommand(stm, connection);
                    rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        MessageBox.Show(rdr.GetInt32(0) + ": " + rdr.GetString(1));
                    }

                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error: {0}", ex.ToString());

            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (connection != null)
                {
                    if (CloseConnection() != true)
                    {
                    }
                }

            }
        }
        void Tabela()
        {
            //MySqlConnection sqliteCon = new MySqlConnection(dbConnectionString);
            MySqlConnection connection;
            string server;
            string database;
            string uid;
            string password;


            server = "192.168.1.115";
            database = "test";
            uid = "test";
            password = "1";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);

            //open connection to database
            try
            {
                connection.Open();
                string Query = "select * from test";
                MySqlCommand createCommand = new MySqlCommand(Query, connection);
                createCommand.ExecuteNonQuery();

                MySqlDataReader rdr = null;
                rdr = createCommand.ExecuteReader();

                while (rdr.Read())
                {

                    MessageBox.Show(rdr.GetInt32(0) + ": "
                        + rdr.GetString(1));
                }
                MySqlDataAdapter dataAdp = new MySqlDataAdapter(createCommand);
                DataTable dt = new DataTable("test");
                dataAdp.Fill(dt);
                //  dataGrid.ItemsSource = dt.DefaultView;
                dataAdp.Update(dt);
                connection.Close();

            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
        ////Select statement
        //public List<string>[] Select()
        //{
        //}
    }
    //
    // DB Login
    //
    class DBConnectLogin
    {
        public Boolean UserConect;
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;


        private int id;
        private string usrlogin;
        private string usrpassword;

        //Constructor
        public DBConnectLogin(string DBUser, string DBPassword, string DBPerfil)
        {
            // Initialize();
            //MessageBox.Show("1");
            LerUsuario(DBUser, DBPassword, DBPerfil);
        }
        //Initialize values
        private void Initialize()
        {

            //// ip local        //server = "127.0.0.1";
            //database = "mydb";
            //uid = "player";
            //password = "player";
            //// ler o Ip do servidor de banco de dados
            //ConfigFile ConfigFile1 = new ConfigFile();
            //ConfigFile1.readfile();
            //server = ConfigFile1.ConfigGravado[0];

            //string connectionString;
            //connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            //database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            // Grid_Click();
            //connection = new MySqlConnection(connectionString);
        }
        void LerUsuario(string _DBUser, string _DBPassword, string _DBPerfil)
        {
            //string cs = @"server=192.168.1.114;userid=player;
            //password=player;database=mydb";

            //// ip local  
            server = "127.0.0.1"; // inicial com ip local.
            database = "mydb";
            uid = "player";
            password = "player";
            // ler o Ip do servidor de banco de dados
            ConfigFile ConfigFile1 = new ConfigFile();
            ConfigFile1.readfile();
            server = ConfigFile1.ConfigGravado[0];
            string cs;
            cs = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "userid=" + uid + ";" + "PASSWORD=" + password + ";";

            MySqlConnection conn = null;
            MySqlDataReader rdr = null;

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();

                string stm = "SELECT nome,senha,perfil FROM user where nome='@nome' and senha='@senha' and perfil='@perfil'";
                stm = stm.Replace("@nome", _DBUser).Replace("@senha", _DBPassword).Replace("@perfil", _DBPerfil);
                MySqlCommand cmd = new MySqlCommand(stm, conn);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    // MessageBox.Show(rdr.GetInt32(0) + ": "
                    //     + rdr.GetString(1));
                    MessageBox.Show("Seja bem vindo " + _DBUser);
                    UserConect = true;

                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.ToString());

            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (conn != null)
                {
                    conn.Close();
                }

            }
        }
        //open connection to database
        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }
        //    //Close connection
        //    private bool CloseConnection()
        //    {
        //        try
        //        {
        //            connection.Close();
        //            return true;
        //        }
        //        catch (MySqlException ex)
        //        {
        //            MessageBox.Show(ex.Message);
        //            return false;
        //        }
        //    }

        //    //Insert statement
        //    public void Insert()
        //    {
        //        string query = "INSERT INTO teste (name, age) VALUES('John Smith', '33')";

        //        //open connection
        //        if (this.OpenConnection() == true)
        //        {
        //            //create command and assign the query and connection from the constructor
        //            MySqlCommand cmd = new MySqlCommand(query, connection);

        //            //Execute command
        //            cmd.ExecuteNonQuery();

        //            //close connection
        //            this.CloseConnection();
        //        }
        //    }

        //    // Ler usuario

        //    public int LerUsuario()
        //    {
        //        string query = "SELECT Count(*) FROM teste";
        //        int Count = -1;

        //        //Open Connection
        //        if (this.OpenConnection() == true)
        //        {
        //            //Create Mysql Command
        //            MySqlCommand cmd = new MySqlCommand(query, connection);

        //            //ExecuteScalar will return one value
        //            Count = int.Parse(cmd.ExecuteScalar() + "");

        //            //close Connection
        //            this.CloseConnection();

        //            return Count;
        //        }
        //        else
        //        {
        //            return Count;
        //        }
        //    }

        //    ////Update statement
        //    //public void Update()
        //    //{
        //    //}

        //    ////Delete statement
        //    //public void Delete()
        //    //{
        //    //}

        //    ////Select statement
        //    //public List<string>[] Select()
        //    //{
        //    //}

        //    ////Count statement
        //    public int Count()
        //    {
        //        string query = "SELECT Count(*) FROM teste";
        //        int Count = -1;

        //        //Open Connection
        //        if (this.OpenConnection() == true)
        //        {
        //            //Create Mysql Command
        //            MySqlCommand cmd = new MySqlCommand(query, connection);

        //            //ExecuteScalar will return one value
        //            Count = int.Parse(cmd.ExecuteScalar() + "");

        //            //close Connection
        //            this.CloseConnection();

        //            return Count;
        //        }
        //        else
        //        {
        //            return Count;
        //        }

        //    }

        //    ////Backup
        //    //public void Backup()
        //    //{
        //    //}

        //    ////Restore
        //    //public void Restore()
        //    //{
        //    //}

        //    void BConnect_click()
        //    {
        //        DBConnect DBConnect1 = new DBConnect();
        //        int count2 = DBConnect1.Count();
        //        MessageBox.Show(count2.ToString());

        //    }
        //    void Grid_Click()
        //    {
        //        //MySqlConnection sqliteCon = new MySqlConnection(dbConnectionString);
        //        MySqlConnection connection;
        //        string server;
        //        string database;
        //        string uid;
        //        string password;


        //        server = "192.168.1.115";
        //        database = "test";
        //        uid = "test";
        //        password = "1";
        //        string connectionString;
        //        connectionString = "SERVER=" + server + ";" + "DATABASE=" +
        //        database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

        //        connection = new MySqlConnection(connectionString);

        //        //open connection to database
        //        try
        //        {
        //            connection.Open();
        //            string Query = "select * from test";
        //            MySqlCommand createCommand = new MySqlCommand(Query, connection);
        //            createCommand.ExecuteNonQuery();

        //            MySqlDataReader rdr = null;
        //            rdr = createCommand.ExecuteReader();

        //            while (rdr.Read())
        //            {

        //                MessageBox.Show(rdr.GetInt32(0) + ": "
        //                    + rdr.GetString(1));
        //            }
        //            MySqlDataAdapter dataAdp = new MySqlDataAdapter(createCommand);
        //            DataTable dt = new DataTable("test");
        //            dataAdp.Fill(dt);
        //            //  dataGrid.ItemsSource = dt.DefaultView;
        //            dataAdp.Update(dt);
        //            connection.Close();

        //        }
        //        catch (MySqlException ex)
        //        {
        //            MessageBox.Show(ex.Message);

        //        }
        //    }
    }
    //
    // DB Evento
    //
    public class DBEvento : DBConnect
    {
        private static string m_perfil = "";
        int position = -1;
        //variaveis do controle  do evento para gravar no DB
        public Boolean backforward = false;
        public int status;
        public int cadastro_video_id;
        public int evento_id;
        public double evento_tempo1;
        public double evento_tempo2;
        public int eventos_status;
        public int foto_id1;
        public string foto_arquivo1;
        public string foto_path1;
        public string foto_camera1;
        public int foto_id2 = 1;
        public string foto_arquivo2;
        public string foto_path2;
        public string foto_camera2;
        public int cadastro_grupo_id;
        public int cadastro_video_id1 = 1;
        public int cadastro_video_grupo_id1;
        public string cadastro_video_arquivo1;
        public string cadastro_video_path1;
        public double cadastro_video_gps_latidute1;
        public double cadastro_video_gps_longitude1;
        public DateTime cadastro_video_hora_inicio1;
        public DateTime cadastro_video_hora_final1;
        public string cadastro_video_camera1;
        public int cadastro_video_frente_tras1;
        public int cadastro_video_id2;
        public int cadastro_video_grupo_id2;
        public string cadastro_video_arquivo2;
        public string cadastro_video_path2;
        public double cadastro_video_gps_latitude2;
        public double cadastro_video_gps_longitude2;
        public DateTime cadastro_video_hora_inicio2;
        public DateTime cadastro_video_hora_final2;
        public string cadastro_video_camera2;
        public int cadastro_video_frente_tras2;

        public string tipos_de_veiculos_id;
        public string tipos_de_veiculos_nome;
        public string tipos_de_veiculos_tipo_de_veiculo;
        public string tipos_de_veiculos_eixo;
        public string eixo_no_solo_id;
        public string eixo_no_solo_eixo_no_solo;
        public string ponto;

        public string usuario_nome = "1";
        public int projeto_id = 1;
        public string projeto_projeto = "1";
        ////IEnumerator and IEnumerable require these methods.
        //public IEnumerator GetEnumerator()
        //{
        //    return (IEnumerator)this;
        //}

        ////IEnumerator
        //public bool MoveNext()
        //{
        //    position++;
        //    return (position < 26);
        //}

        ////IEnumerable
        //public void Reset()
        //{ position = 0; }

        ////IEnumerable
        //public object Current
        //{
        // //   get { return carlist[position]; }
        //    get { return 1; }
        //}

        // 
        //DB Eventos
        //
        public void EventoInsert()
        {
            try
            {

                string query = "INSERT INTO `eventos` (";
                query = query + "`id`, ";
                query = query + "`cadastro_grupo_id`, ";
                query = query + "`evento_tempo1`, ";
                query = query + "`evento_tempo2`, ";
                query = query + "`status`, ";
                query = query + "`foto_id1`, ";
                query = query + "`foto_arquivo1`, ";
                query = query + "`foto_path1`, ";
                query = query + "`foto_camera1`, ";
                query = query + "`foto_id2`, ";
                query = query + "`foto_arquivo2`, ";
                query = query + "`foto_path2`, ";
                query = query + "`foto_camera2`, ";
                query = query + "`cadastro_video_id1`, ";
                query = query + "`cadastro_video_arquivo1`, ";
                query = query + "`cadastro_video_path1`, ";
                query = query + "`cadastro_video_gps_latidute1`, ";
                query = query + "`cadastro_video_gps_longitude1`, ";
                query = query + "`cadastro_video_hora_inicio1`, ";
                query = query + "`cadastro_video_hora_final1`, ";
                query = query + "`cadastro_video_camera1`, ";
                query = query + "`cadastro_video_frente_tras1`, ";
                query = query + "`cadastro_video_id2`, ";
                query = query + "`cadastro_video_arquivo2`, ";
                query = query + "`cadastro_video_path2`, ";
                query = query + "`cadastro_video_gps_latitude2`, ";
                query = query + "`cadastro_video_gps_longitude2`, ";
                query = query + "`cadastro_video_hora_final2`, ";
                query = query + "`cadastro_video_hora_inicio2`, ";
                query = query + "`cadastro_video_camera2`, ";
                query = query + "`cadastro_video_frente_tras2`, ";
                query = query + "`projeto_id`, ";
                query = query + "`projeto_projeto`, ";
                query = query + "`tipos_de_veiculos_id`, ";
                query = query + "`tipos_de_veiculos_nome`, ";
                query = query + "`tipos_de_veiculos_tipo_de_veiculo`, ";
                query = query + "`tipos_de_veiculos_eixo`, ";
                query = query + "`eixo_no_solo_id`, ";
                query = query + "`eixo_no_solo_eixo_no_solo`, ";
                query = query + "`usuario_nome`";
                query = query + ") VALUES (";
                query = query + "NULL, ";
                query = query + "'" + cadastro_grupo_id + "', ";
                query = query + "'" + evento_tempo1 + "', ";
                query = query + "'" + evento_tempo2 + "', ";
                query = query + "'" + status + "', ";
                query = query + "'" + foto_id1 + "', ";
                query = query + "'" + foto_arquivo1 + "', ";
                query = query + "'" + foto_path1 + "', ";
                query = query + "'" + foto_camera1 + "', ";
                query = query + "'" + foto_id2 + "', ";
                query = query + "'" + foto_arquivo2 + "', ";
                query = query + "'" + foto_path2 + "', ";
                query = query + "'" + foto_camera2 + "', ";
                query = query + "'" + cadastro_video_id1 + "', ";
                query = query + "'" + cadastro_video_arquivo1 + "', ";
                query = query + "'" + cadastro_video_path1 + "', ";
                query = query + "'" + cadastro_video_gps_latidute1 + "', ";
                query = query + "'" + cadastro_video_gps_longitude1 + "', ";
                query = query + "'" + cadastro_video_hora_inicio1.ToString("yyyy-MM-dd HH:mm:ss") + "', ";
                query = query + "'" + cadastro_video_hora_final1.ToString("yyyy-MM-dd HH:mm:ss") + "', ";
                query = query + "'" + cadastro_video_camera1 + "', ";
                query = query + "'" + cadastro_video_frente_tras1 + "', ";
                query = query + "'" + cadastro_video_id2 + "', ";
                query = query + "'" + cadastro_video_arquivo2 + "', ";
                query = query + "'" + cadastro_video_path2 + "', ";
                query = query + "'" + cadastro_video_gps_latitude2 + "', ";
                query = query + "'" + cadastro_video_gps_longitude2 + "', ";
                query = query + "'" + cadastro_video_hora_final2.ToString("yyyy-MM-dd HH:mm:ss") + "', ";
                query = query + "'" + cadastro_video_hora_inicio2.ToString("yyyy-MM-dd HH:mm:ss") + "', ";
                query = query + "'" + cadastro_video_camera2 + "', ";
                query = query + "'" + cadastro_video_frente_tras2 + "', ";
                query = query + "'" + projeto_id + "', ";
                query = query + "'" + projeto_projeto + "', ";
                query = query + "'" + tipos_de_veiculos_id + "', ";
                query = query + "'" + tipos_de_veiculos_nome + "', ";
                query = query + "'" + tipos_de_veiculos_tipo_de_veiculo + "', ";
                query = query + "'" + tipos_de_veiculos_eixo + "', ";
                query = query + "'" + eixo_no_solo_id + "', ";
                query = query + "'" + eixo_no_solo_eixo_no_solo + "', ";
                query = query + "'" + usuario_nome + "'";
                query = query + ")";

                connection = new MySqlConnection(connectionString);
                //open connection
                if (this.OpenConnection() == true)
                {
                    //create command and assign the query and connection from the constructor
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    //Execute command
                    cmd.ExecuteNonQuery();

                    //close connection
                    this.CloseConnection();
                }

            }
            catch
            {
                MessageBox.Show("Error:");
            }
        }
        public void EventoApagar(int evento_id)
        {
            MySqlDataReader rdr = null;
            try
            {
                connection = new MySqlConnection(connectionString);
                if (OpenConnection() == true)
                {
                    string stm = "DELETE FROM eventos WHERE id ='";
                    stm = stm  + evento_id + "'";
                    MySqlCommand cmd = new MySqlCommand(stm, connection);
                    object result = cmd.ExecuteNonQuery();
                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error: {0}", ex.ToString());

            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (connection != null)
                {
                    if (CloseConnection() != true)
                    {
                    }
                }

            }
        }
        public void EventoCadastroVideo()
        {
            MySqlDataReader rdr = null;

            try
            {
                connection = new MySqlConnection(connectionString);
                if (OpenConnection() == true)
                {

                    string stm = "SELECT id , grupo_id, projeto_id ,ponto , status , arquivo ,path";
                    stm = stm + " , gps_latitude, gps_longitude, hora_inicio ,hora_final";
                    stm = stm + " , camera , frente_tras  FROM `view_cadastro_video` ";
                    stm = stm + " WHERE grupo_id = '" + cadastro_grupo_id + "' ";
                    stm = stm + "ORDER BY `view_cadastro_video`.`grupo_id` ASC LIMIT 2";
                    MySqlCommand cmd = new MySqlCommand(stm, connection);
                    rdr = cmd.ExecuteReader();
                    int camera = 1;
                    while (rdr.Read())
                    {
                        if (camera == 1)
                        {
                            cadastro_video_id1 = rdr.GetInt32(0);
                            cadastro_video_grupo_id1 = rdr.GetInt32(1);
                            //2 projeto id 
                            //3 ponto
                            //4 status
                            cadastro_video_arquivo1 = rdr.GetString(5);
                            cadastro_video_path1 = rdr.GetString(6).Replace("\\","\\\\");
                            cadastro_video_gps_latidute1 = rdr.GetDouble(7);
                            cadastro_video_gps_longitude1 = rdr.GetDouble(8);
                            cadastro_video_hora_inicio1 = rdr.GetDateTime(9);
                            cadastro_video_hora_final1 = rdr.GetDateTime(10);
                            cadastro_video_camera1 = rdr.GetString(11);
                            cadastro_video_frente_tras1 = rdr.GetInt16(12);
                        }
                        if (camera == 2)
                        {
                            cadastro_video_id2 = rdr.GetInt32(0);
                            cadastro_video_grupo_id2 = rdr.GetInt32(1);
                            //2 projeto id 
                            //3 ponto
                            //4 status
                            cadastro_video_arquivo2 = rdr.GetString(5);
                            cadastro_video_path2 = rdr.GetString(6).Replace("\\", "\\\\");
                            cadastro_video_gps_latitude2 = rdr.GetDouble(7);
                            cadastro_video_gps_longitude2 = rdr.GetDouble(8);
                            cadastro_video_hora_inicio2 = rdr.GetDateTime(9);
                            cadastro_video_hora_final2 = rdr.GetDateTime(10);
                            cadastro_video_camera2 = rdr.GetString(11);
                            cadastro_video_frente_tras2 = rdr.GetInt16(12);
                        }
                        camera++;
                    }

                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error: {0}", ex.ToString());

            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (connection != null)
                {
                    if (CloseConnection() != true)
                    {
                    }
                }

            }
        }
        public void EventoProjeto_id(int cadastrogrupo_id)
        {
            MySqlDataReader rdr = null;
            try
            {
                connection = new MySqlConnection(connectionString);
                if (OpenConnection() == true)
                {
                    string stm = "SELECT projeto_id FROM `view_cadastro_video` ";
                    stm = stm + " WHERE grupo_id = '" + cadastrogrupo_id + "' LIMIT 1";
                    MySqlCommand cmd = new MySqlCommand(stm, connection);
                    object result = cmd.ExecuteScalar();
                    if (result != null)
                    {
                        projeto_id = Convert.ToInt32(result);
                    }

                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error: {0}", ex.ToString());

            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (connection != null)
                {
                    if (CloseConnection() != true)
                    {
                    }
                }

            }

        }
        public void EventoPonto(int cadastrogrupo_id)
        {
            MySqlDataReader rdr = null;
            try
            {
                connection = new MySqlConnection(connectionString);
                if (OpenConnection() == true)
                {
                    string stm = "SELECT ponto FROM `view_cadastro_video` ";
                    stm = stm + " WHERE grupo_id = '" + cadastrogrupo_id + "' LIMIT 1";
                    MySqlCommand cmd = new MySqlCommand(stm, connection);
                    object result = cmd.ExecuteScalar();
                    if (result != null)
                    {
                        ponto = Convert.ToString(result);
                    }

                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error: {0}", ex.ToString());

            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (connection != null)
                {
                    if (CloseConnection() != true)
                    {
                    }
                }

            }

        }
        public void EventoProjetoNome()
        {

            MySqlDataReader rdr = null;

            try
            {
                connection = new MySqlConnection(connectionString);
                if (OpenConnection() == true)
                {
                    string stm = "SELECT id , projeto, status ";
                    stm = stm + "FROM `projeto` ";
                    stm = stm + "WHERE id='" + projeto_id+ "'";
                    MySqlCommand cmd = new MySqlCommand(stm, connection);
                    rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        //projeto_id = rdr.GetInt32(0);
                        projeto_projeto =  rdr.GetString(1);
                        //2
                    }

                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error: {0}", ex.ToString());

            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (connection != null)
                {
                    if (CloseConnection() != true)
                    {
                    }
                }

            }


        }
        public void EventoUsuario()
        {
            usuario_nome = Usuario.Login;
        }
        public void EventosStatus()
        { // define o status como em andamento
           status = 1;
        }
    }
    //
    // DB para criacao dinamica dos botoes no formulario Eventos
    //
    public class DBButton
    {
        public string KeyChar1;
        public string Keydata1;
        //  arrays para os botoes
        public int[] projeto_id = new int[] { 1 } ;
        public string[] classificacao =new string[] { ""};
        public int[] painel = new int[] { 1 };
        public int[] PontoX = new int[] { 1 };
        public int[] PontoY = new int[] { 1 };
        public string[] Atalho = new string[] { "" };
        // variaveis de banco de dados
        private string server;
        private string database;
        private string uid;
        private string password;
        //
        // altera o tamanho e conteudo dos arrays conforme o banco de dados
        //
        public void DBButton_ler_dados(int _projeto_id)
        {
            //string cs = @"server=192.168.1.114;userid=player;
            //password=player;database=mydb";

            //// ip local  
            server = "127.0.0.1"; // inicial com ip local.
            database = "mydb";
            uid = "player";
            password = "player";
            // ler o Ip do servidor de banco de dados
            ConfigFile ConfigFile1 = new ConfigFile();
            ConfigFile1.readfile();
            server = ConfigFile1.ConfigGravado[0];
            string cs;
            cs = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "userid=" + uid + ";" + "PASSWORD=" + password + ";";

            MySqlConnection conn = null;
            MySqlDataReader rdr = null;

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
                string stm = "SELECT `id`, `projeto_id`, `classicacao`, `painel`, `PontoX`, `PontoY`, `Atalho` from classificacao  where projeto_id='@projeto_id' ";
                stm = stm.Replace("@projeto_id", _projeto_id.ToString());
                MySqlCommand cmd = new MySqlCommand(stm, conn);
                rdr = cmd.ExecuteReader();
                int i = 0;
                while (rdr.Read())
                {
                    Array.Resize(ref projeto_id, i + 1);
                    Array.Resize(ref classificacao, i + 1);
                    Array.Resize(ref painel, i + 1);
                    Array.Resize(ref PontoX, i + 1);
                    Array.Resize(ref PontoY, i + 1);
                    Array.Resize(ref Atalho, i + 1);

                    projeto_id[i] = rdr.GetInt32(1);
                    classificacao[i] = rdr.GetString(2);
                    painel[i] = rdr.GetInt32(3);
                    PontoX[i] = rdr.GetInt32(4);
                    PontoY[i] = rdr.GetInt32(5);
                    Atalho[i] = rdr.GetString(6);

                    i++;
                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.ToString());

            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (conn != null)
                {
                    conn.Close();
                }

            }
        }
    }
    //
    // TXT do IP do DB
    //
    class ConfigFile
    {
        public Boolean ConfigFileTrue = false;
        public string PathOfExec;
        public string PathOfConfig;
        public string[] ConfigGravado = new string[] { "127.0.0.1","c:\\"};

        public ConfigFile()
        {
            PathOfExec = this.PathExe();
            ConfigFileTrue = ConfigFileExist();
        }
        private string PathExe()
        {
            string path = System.AppDomain.CurrentDomain.BaseDirectory.ToString();
            string path2 = path.Remove(path.LastIndexOf("\\"));
            path = path2.Remove(path2.LastIndexOf("\\") + 1);
            return path;

        }
        public Boolean ConfigFileExist()
        {
            Boolean fileExist;
            string path1;
            PathOfConfig = PathOfExec + "config.txt";
            fileExist = File.Exists(PathOfConfig);
            //fileExist = File.Exists("C:\\config.txt");
            return fileExist;
        }
        public void writefile(string[] parametros)
        {
            string path = PathOfConfig;
            try {
                // This text is added only once to the file.
                //if (!File.Exists(path))
                // {
                // Create a file to write to.
                string createText = parametros[0] + Environment.NewLine + parametros[1] + Environment.NewLine;
                File.WriteAllText(path, createText);
                ConfigGravado[0]= parametros[0];
                ConfigGravado[1] = parametros[1];
            }
            catch
            {
                MessageBox.Show("Arquivo config.txt nao gravado");
            };
            // }

            // This text is always added, making the file longer over time
            // if it is not deleted.
            // string appendText = parametros[1] + Environment.NewLine;
            // File.AppendAllText(path, appendText);

            // Open the file to read from.
            // string readText = File.ReadAllText(path);
            // Console.WriteLine(readText);
        }
        public void readfile()
        {
            string readText = "1";
            if (ConfigFileTrue)
            {
                string[] lines = System.IO.File.ReadAllLines(PathOfConfig);
                int i = 0;
                foreach (string line in lines)
                {

                    // Use a tab to indent each line of the file.
                    // MessageBox.Show(line); 
                    Array.Resize(ref ConfigGravado, i + 1);
                    ConfigGravado[i] = line;
                    i++;
                }

            }
        }
    }
}
