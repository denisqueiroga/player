﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class ConfiguracaoCadastroProjetos : System.Windows.Forms.Form
    {
        int dataadd = 0;

        private MySqlDataAdapter[] da;        // Data Adapter
        private DataSet[] ds;                 // Dataset
        private string [] sTable;  // Table Name
        private bool[] salvar;
        //
        // DB ler, update e salvar
        //        
        public ConfiguracaoCadastroProjetos()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;

            for (int i = 0; i < 4; i++)
            { 
                Array.Resize(ref da, i+1);
                Array.Resize(ref ds, i+1);
                Array.Resize(ref sTable, i+1);
                Array.Resize(ref salvar, i+1);
                switch (i)
                {
                    case 0:
                        sTable[i] = "projeto";
                        break;
                    case 1:
                        sTable[i] = "classificacao";
                        break;
                    case 2:
                        sTable[i] = "cadastro_video";
                        break;
                    case 3:
                        sTable[i] = "grupo";
                        break;
                }

                Loadtable(i);
                salvar[i] = false;
            }

            
            
        }
        private BindingSource Load1()
        {
            
            //MySqlConnection sqliteCon = new MySqlConnection(dbConnectionString);
            MySqlConnection connection;
            string server;
            string database;
            string userid;
            string password;


            // server = "192.168.1.106";
            server = Usuario.IPDB;
            database = "mydb";
            userid = "player";
            password = "player";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "USERID=" + userid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
            DataSet ds = new DataSet();
            BindingSource bs = new BindingSource();

            //open connection to database
            connection.Open();

            //select statament
            string Query = "select * from projeto";
            MySqlCommand cmd = new MySqlCommand(Query, connection);
            //Adapter
            // DataTable dataTable = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
            adapter.SelectCommand = cmd;
            adapter.Fill(ds);
            bs.DataSource = ds.Tables[0];
            return bs;
        }
        private void Loadtable(int i)
        {
            string server;
            string database;
            string userid;
            string password;

            server = Usuario.IPDB;
            database = "mydb";
            userid = "player";
            password = "player";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "USERID=" + userid + ";" + "PASSWORD=" + password + ";Allow Zero Datetime = true;";

            MySqlConnection conn = null;

            try
            {
                conn = new MySqlConnection(connectionString);

                conn.Open();
                switch (i)
                {
                    case 0:
                        da[i] = new MySqlDataAdapter("SELECT * FROM projeto;", conn);
                        break;
                    case 1:
                        da[i] = new MySqlDataAdapter("SELECT * FROM classificacao;", conn);
                        break;
                    case 2:
                        da[i] = new MySqlDataAdapter("SELECT * FROM cadastro_video;", conn);
                        break;
                    case 3:
                        da[i] = new MySqlDataAdapter("SELECT * FROM grupo_video;", conn);
                        break;
                }
                ds[i] = new DataSet();
                da[i].Fill(ds[i], sTable[i]);
                conn.Close();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
            finally
            {
                switch (i)
                {
                    case 0:
                        dataGridView1.Refresh();

                        dataGridView1.DataSource = ds[i];
                        dataGridView1.DataMember = sTable[i];
                        dataGridView1.Columns["id"].ReadOnly = true;

                        break;
                    case 1:
                        dataGridView2.Refresh();

                        dataGridView2.DataSource = ds[i];
                        dataGridView2.DataMember = sTable[i];
                        dataGridView2.Columns["id"].ReadOnly = true;
                        break;
                    case 2:
                        dataGridView3.Refresh();

                        dataGridView3.DataSource = ds[i];
                        dataGridView3.DataMember = sTable[i];
                        dataGridView3.Columns["id"].ReadOnly = true;
                        break;
                    case 3:
                        dataGridView4.Refresh();

                        dataGridView4.DataSource = ds[i];
                        dataGridView4.DataMember = sTable[i];
                        dataGridView4.Columns["id"].ReadOnly = true;
                        break;
                }
                salvar[i] = false;
            }
        }
        private void Update(int i)
        {
            string server;
            string database;
            string userid;
            string password;


            // server = "192.168.1.106";
            server = Usuario.IPDB;
            database = "mydb";
            userid = "player";
            password = "player";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "USERID=" + userid + ";" + "PASSWORD=" + password + ";";
            try
            {
                MySqlConnection conn = new MySqlConnection(connectionString);
                conn.Open();

                MySqlCommandBuilder cmb = new MySqlCommandBuilder(da[i]);
                //cmb.Connection = conn;
                da[i].Update(ds[i], sTable[i]);
                salvar[i] = false;
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
               // if (conn.State == System.Data.ConnectionState.Open)
               //     conn.Close();
            }
        }
        private void delete(int i)
        {
            switch (i)
            {
                case 0:
                    foreach (DataGridViewRow item in this.dataGridView1.SelectedRows)
                    {
                        dataGridView1.Rows.RemoveAt(item.Index);
                        salvar[i] = true;
                    }
                    break;
                case 1:
                    foreach (DataGridViewRow item in this.dataGridView2.SelectedRows)
                    {
                        dataGridView2.Rows.RemoveAt(item.Index);
                        salvar[i] = true;
                    }
                    break;
                case 2:
                    foreach (DataGridViewRow item in this.dataGridView3.SelectedRows)
                    {
                        dataGridView3.Rows.RemoveAt(item.Index);
                        salvar[i] = true;
                    }
                    break;
                case 3:
                    foreach (DataGridViewRow item in this.dataGridView4.SelectedRows)
                    {
                        dataGridView4.Rows.RemoveAt(item.Index);
                        salvar[i] = true;
                    }
                    break;

            }
        }
        //
        // sair
        //
        private void buttonvideosair_Click(object sender, EventArgs e)
        {
            Sair();
        }
        private void buttonprojetosair_Click(object sender, EventArgs e)
        {
            Sair();
        }
        private void buttonclassificacaosair_Click(object sender, EventArgs e)
        {
            Sair();
        }
        private void buttongruposair_Click(object sender, EventArgs e)
        {
            Sair();
        }
        private void Sair()
        {
            DialogResult fechar;
            string mensagem1;
            mensagem1 = "existem dados para salvar realmente deseja sair!";
            if (salvar[0] || salvar[1] || salvar[2])
            {
                if (salvar[0])
                    mensagem1 = mensagem1 + "\n\n 1 - dados a salvar da tabela projetos";
                if (salvar[1])
                    mensagem1 = mensagem1 + "\n\n 2 - dados a salvar da tabela classificacao";
                if (salvar[2])
                    mensagem1 = mensagem1 + "\n\n 3 - dados a salvar da tabela videos";
                fechar = MessageBox.Show(mensagem1, "Sair",  MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (fechar == DialogResult.Yes)
                {
                    this.Close();
                }
                //seu codigo se clica no yes
                else
                {
                    //seu codigo se clicar No
                }
            }
            else  // nao tem nada para salvar
            {
                this.Close();
            } 
             
            
        }
        //
        // salvar
        //
        private void buttonprojetosalvar_Click(object sender, EventArgs e)
        {
            DialogResult fechar;
            fechar = MessageBox.Show("Salvar as altearçoes na tabela cadastro de projetos !", "Salvar", MessageBoxButtons.YesNo);
            if (fechar == DialogResult.Yes)
            {
                Update(0);
            }
            //seu codigo se clica no yes
            else
            {
                //seu codigo se clicar No
            }

        }
        private void buttonclassificacaosalvar_Click(object sender, EventArgs e)
        {
            DialogResult fechar;
            fechar = MessageBox.Show("Salvar as altearçoes na tabela cadastro de classificacao !", "Salvar", MessageBoxButtons.YesNo);
            if (fechar == DialogResult.Yes)
            {
                Update(1);
            }
            //seu codigo se clica no yes
            else
            {
                //seu codigo se clicar No
            }

        }
        private void buttonvideosalvar_Click(object sender, EventArgs e)
        {
            DialogResult fechar;
            fechar = MessageBox.Show("Salvar as altearçoes na tabela cadastro de videos !", "Salvar", MessageBoxButtons.YesNo);
            if (fechar == DialogResult.Yes)
            {
                Update(2);
            }
            //seu codigo se clica no yes
            else
            {
                //seu codigo se clicar No
            }
        }
        private void buttongruposalvar_Click(object sender, EventArgs e)
        {
            DialogResult fechar;
            fechar = MessageBox.Show("Salvar as altearçoes na tabela grupo video !", "Salvar", MessageBoxButtons.YesNo);
            if (fechar == DialogResult.Yes)
            {
                Update(3);
            }
            //seu codigo se clica no yes
            else
            {
                //seu codigo se clicar No
            }
        }
        //
        // refresh
        //
        private void buttonprojetorefresh_Click(object sender, EventArgs e)
        {
            DialogResult fechar;
            fechar = MessageBox.Show("Atualizar  !", "Atualizar", MessageBoxButtons.YesNo);
            if (fechar == DialogResult.Yes)
            {
                Loadtable(0);
            }
            //seu codigo se clica no yes
            else
            {
                //seu codigo se clicar No
            }
        }
        private void buttonclassificacaorefresh_Click(object sender, EventArgs e)
        {
            DialogResult fechar;
            fechar = MessageBox.Show("Atualizar  !", "Atualizar", MessageBoxButtons.YesNo);
            if (fechar == DialogResult.Yes)
            {
                Loadtable(1);
            }
            //seu codigo se clica no yes
            else
            {
                //seu codigo se clicar No
            }
        }
        private void buttonvideoreflesh_Click(object sender, EventArgs e)
        {
            DialogResult fechar;
            fechar = MessageBox.Show("Atualizar  !", "Atualizar", MessageBoxButtons.YesNo);
            if (fechar == DialogResult.Yes)
            {
                Loadtable(2);
            }
            //seu codigo se clica no yes
            else
            {
                //seu codigo se clicar No
            }
        }
        private void buttongruporefresh_Click(object sender, EventArgs e)
        {
            DialogResult fechar;
            fechar = MessageBox.Show("Atualizar  !", "Atualizar", MessageBoxButtons.YesNo);
            if (fechar == DialogResult.Yes)
            {
                Loadtable(3);
            }
            //seu codigo se clica no yes
            else
            {
                //seu codigo se clicar No
            }

        }
        //
        // apagar
        //
        private void buttonprojetoapagar_Click(object sender, EventArgs e)
        {
            DialogResult fechar;
            fechar = MessageBox.Show("Apagar a linhas selecionadas do projeto!\n Salve depois para efetivar a alteracao", "Apagar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (fechar == DialogResult.Yes)
            {
                delete(0);
            }
            //seu codigo se clica no yes
            else
            {
                //seu codigo se clicar No
            }
        }
        private void buttonclassificacaoapagar_Click(object sender, EventArgs e)
        {
            DialogResult fechar;
            fechar = MessageBox.Show("Apagar a linhas selecionadas da classificacao!\n Salve depois para efetivar a alteracao", "Apagar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (fechar == DialogResult.Yes)
            {
                delete(1);
            }
            //seu codigo se clica no yes
            else
            {
                //seu codigo se clicar No
            }
        }
        private void buttonvideoapagar_Click(object sender, EventArgs e)
        {
            DialogResult fechar;
            fechar = MessageBox.Show("Apagar a linhas selecionadas!\n Salve depois para efetivar a alteracao", "Apagar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (fechar == DialogResult.Yes)
            {
                delete(2);
            }
            //seu codigo se clica no yes
            else
            {
                //seu codigo se clicar No
            }
        }
        private void buttongrupoapagar_Click(object sender, EventArgs e)
        {
            DialogResult fechar;
            fechar = MessageBox.Show("Apagar a linhas selecionadas!\n Salve depois para efetivar a alteracao", "Apagar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (fechar == DialogResult.Yes)
            {
                delete(3);
            }
            //seu codigo se clica no yes
            else
            {
                //seu codigo se clicar No
            }

        }
        //
        // variavel de salvar
        //
        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            salvar[0] = true;
        }
        private void dataGridView2_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {


            salvar[1] = true;
        }
        private void dataGridView3_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            salvar[2] = true;
        }
        private void dataGridView4_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            salvar[3] = true;
        }
    }
}
