﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class ProjetoEventoPublicar : System.Windows.Forms.Form
    {

        // variavel de evento com conecxao de banco de dados.
        DBEvento DBEvento1 = new DBEvento();


        public ProjetoEventoPublicar(int cadastrogrupo_id)
        {
            InitializeComponent();
            //
            // inicializa o DB com o grupo_id
            //
            DBEvento1.cadastro_grupo_id = cadastrogrupo_id;

            //
            // ler a tabela de dados
            //
            dataGridView1.DataSource = Load();

        }
        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
        private BindingSource Load()
        {
            //MySqlConnection sqliteCon = new MySqlConnection(dbConnectionString);
            MySqlConnection connection;
            string server;
            string database;
            string userid;
            string password;


            // server = "192.168.1.106";
            server = Usuario.IPDB;
            database = "mydb";
            userid = "player";
            password = "player";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "USERID=" + userid + ";" + "PASSWORD=" + password + ";" + " convert zero datetime=True;";

            connection = new MySqlConnection(connectionString);
            DataSet ds = new DataSet();
            BindingSource bs = new BindingSource();

            //open connection to database
            connection.Open();

            //select statament
            string Query = "select * from eventos where cadastro_grupo_id= '" + DBEvento1.cadastro_grupo_id + "' ORDER BY `eventos`.`evento_tempo1` DESC ";
            MySqlCommand cmd = new MySqlCommand(Query, connection);
            //Adapter
            // DataTable dataTable = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
            adapter.SelectCommand = cmd;
            adapter.Fill(ds);
            bs.DataSource = ds.Tables[0];
            return bs;
        }

        //
        // Salvar Imagens
        //
        private void button10_Click(object sender, EventArgs e)
        {
            //System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(axWindowsMediaPlayer1.Width, axWindowsMediaPlayer1.Height);
            //axWindowsMediaPlayer1.DrawToBitmap(bmp, axWindowsMediaPlayer1.ClientRectangle);
            //bmp.Save("c: \\ 1.jpg", System.Drawing.Imaging.ImageFormat.Png);
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {



            if ((e.RowIndex >= 0) && (e.ColumnIndex >= 0))
            {
                //proibir edicao
                dataGridView1.Rows[e.RowIndex].ReadOnly = true;
                if (dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null)
                {
                    dataGridView1.Rows[e.RowIndex].ReadOnly = false;
                }
                // ler os dados no painel

                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                    DBEvento1.foto_arquivo1 = row.Cells["foto_arquivo1"].Value.ToString();
                    DBEvento1.foto_arquivo2 = row.Cells["foto_arquivo2"].Value.ToString();
                    DBEvento1.foto_id1 = Convert.ToInt32(row.Cells["foto_id1"].Value);
                    DBEvento1.foto_id2 = Convert.ToInt32(row.Cells["foto_id2"].Value);
                }
                try
                {
                    if (DBEvento1.foto_arquivo1 != "")
                        pictureBox1.Image = Image.FromFile(DBEvento1.foto_arquivo1);
                    if (DBEvento1.foto_arquivo2 != "")
                        pictureBox2.Image = Image.FromFile(DBEvento1.foto_arquivo2);
                }
                catch
                {
                    MessageBox.Show("Erro ao ler a imagem");
                }
            }
        }
    }
}
