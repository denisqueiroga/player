﻿namespace WindowsFormsApplication1
{
    partial class ConfiguracaoConfigurar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonSalvar = new System.Windows.Forms.Button();
            this.labelIPDB = new System.Windows.Forms.Label();
            this.labelCaminhoFotos = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.ButtonCancelar = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // ButtonSalvar
            // 
            this.ButtonSalvar.Location = new System.Drawing.Point(217, 171);
            this.ButtonSalvar.Name = "ButtonSalvar";
            this.ButtonSalvar.Size = new System.Drawing.Size(75, 23);
            this.ButtonSalvar.TabIndex = 0;
            this.ButtonSalvar.Text = "&Salvar";
            this.ButtonSalvar.UseVisualStyleBackColor = true;
            this.ButtonSalvar.Click += new System.EventHandler(this.buttonSalvar_Click);
            // 
            // labelIPDB
            // 
            this.labelIPDB.AutoSize = true;
            this.labelIPDB.Location = new System.Drawing.Point(34, 43);
            this.labelIPDB.Name = "labelIPDB";
            this.labelIPDB.Size = new System.Drawing.Size(90, 13);
            this.labelIPDB.TabIndex = 1;
            this.labelIPDB.Text = "IP do servidor BD";
            // 
            // labelCaminhoFotos
            // 
            this.labelCaminhoFotos.AutoSize = true;
            this.labelCaminhoFotos.Location = new System.Drawing.Point(34, 118);
            this.labelCaminhoFotos.Name = "labelCaminhoFotos";
            this.labelCaminhoFotos.Size = new System.Drawing.Size(97, 13);
            this.labelCaminhoFotos.TabIndex = 3;
            this.labelCaminhoFotos.Text = "Caminhos da Fotos";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(176, 118);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 4;
            // 
            // ButtonCancelar
            // 
            this.ButtonCancelar.Location = new System.Drawing.Point(37, 171);
            this.ButtonCancelar.Name = "ButtonCancelar";
            this.ButtonCancelar.Size = new System.Drawing.Size(75, 23);
            this.ButtonCancelar.TabIndex = 5;
            this.ButtonCancelar.Text = "&Cancelar";
            this.ButtonCancelar.UseVisualStyleBackColor = true;
            this.ButtonCancelar.Click += new System.EventHandler(this.buttonCancelar_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(282, 118);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(31, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.buttonDialogBox_Click);
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(176, 43);
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.ResetOnSpace = false;
            this.maskedTextBox1.Size = new System.Drawing.Size(100, 20);
            this.maskedTextBox1.TabIndex = 7;
            // 
            // ConfiguracaoConfigurar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(325, 221);
            this.Controls.Add(this.maskedTextBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ButtonCancelar);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.labelCaminhoFotos);
            this.Controls.Add(this.labelIPDB);
            this.Controls.Add(this.ButtonSalvar);
            this.Name = "ConfiguracaoConfigurar";
            this.Text = "Configuracao";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonSalvar;
        private System.Windows.Forms.Label labelIPDB;
        private System.Windows.Forms.Label labelCaminhoFotos;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button ButtonCancelar;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
    }
}