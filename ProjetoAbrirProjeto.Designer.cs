﻿namespace WindowsFormsApplication1
{
    partial class ProjetoAbrirProjeto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonevento = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label_usuario = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label_projeto_id = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.id = new System.Windows.Forms.Label();
            this.label_projeto = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label_cadastro_video_ponto = new System.Windows.Forms.Label();
            this.label_ponto = new System.Windows.Forms.Label();
            this.label_cadastro_video_camera = new System.Windows.Forms.Label();
            this.label_camera = new System.Windows.Forms.Label();
            this.label_cadastro_vdeo_data_hora = new System.Windows.Forms.Label();
            this.label_hora_inicio = new System.Windows.Forms.Label();
            this.label_cadastro_video_id = new System.Windows.Forms.Label();
            this.label_cadastro_grupo_id = new System.Windows.Forms.Label();
            this.label_cadastro_videdo_frente_tras = new System.Windows.Forms.Label();
            this.label_grupo_id = new System.Windows.Forms.Label();
            this.label_video_id = new System.Windows.Forms.Label();
            this.label_frente_tras = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonGerarImagens = new System.Windows.Forms.Button();
            this.buttonpublicar = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 57);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(690, 176);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // buttonevento
            // 
            this.buttonevento.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonevento.Location = new System.Drawing.Point(3, 3);
            this.buttonevento.Name = "buttonevento";
            this.buttonevento.Size = new System.Drawing.Size(76, 44);
            this.buttonevento.TabIndex = 4;
            this.buttonevento.Text = " Eventos";
            this.buttonevento.UseVisualStyleBackColor = true;
            this.buttonevento.Click += new System.EventHandler(this.buttonabrir_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(699, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Usuario:";
            // 
            // label_usuario
            // 
            this.label_usuario.AutoSize = true;
            this.label_usuario.Location = new System.Drawing.Point(699, 54);
            this.label_usuario.Name = "label_usuario";
            this.label_usuario.Size = new System.Drawing.Size(69, 13);
            this.label_usuario.TabIndex = 8;
            this.label_usuario.Text = "label_usuario";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 88.88889F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label_usuario, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label6, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView2, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 1, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.455851F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 31.52082F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.194191F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.352602F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.44425F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.8096F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(784, 583);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(690, 48);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Lista de Projetos Cadastrados";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.label_projeto_id, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.id, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label_projeto, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 239);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(690, 35);
            this.tableLayoutPanel2.TabIndex = 10;
            // 
            // label_projeto_id
            // 
            this.label_projeto_id.AutoSize = true;
            this.label_projeto_id.Location = new System.Drawing.Point(519, 0);
            this.label_projeto_id.Name = "label_projeto_id";
            this.label_projeto_id.Size = new System.Drawing.Size(81, 13);
            this.label_projeto_id.TabIndex = 8;
            this.label_projeto_id.Text = "label_projeto_id";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "projeto:";
            // 
            // id
            // 
            this.id.AutoSize = true;
            this.id.Location = new System.Drawing.Point(347, 0);
            this.id.Name = "id";
            this.id.Size = new System.Drawing.Size(42, 13);
            this.id.TabIndex = 6;
            this.id.Text = "projeto:";
            // 
            // label_projeto
            // 
            this.label_projeto.AutoSize = true;
            this.label_projeto.Location = new System.Drawing.Point(175, 0);
            this.label_projeto.Name = "label_projeto";
            this.label_projeto.Size = new System.Drawing.Size(67, 13);
            this.label_projeto.TabIndex = 0;
            this.label_projeto.Text = "label_projeto";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 486);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(690, 94);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Projeto Selecionado";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 8;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel3.Controls.Add(this.label_cadastro_video_ponto, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.label_ponto, 4, 1);
            this.tableLayoutPanel3.Controls.Add(this.label_cadastro_video_camera, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label_camera, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.label_cadastro_vdeo_data_hora, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label_hora_inicio, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.label_cadastro_video_id, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label_cadastro_grupo_id, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label_cadastro_videdo_frente_tras, 5, 0);
            this.tableLayoutPanel3.Controls.Add(this.label_grupo_id, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label_video_id, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label_frente_tras, 5, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(684, 75);
            this.tableLayoutPanel3.TabIndex = 12;
            // 
            // label_cadastro_video_ponto
            // 
            this.label_cadastro_video_ponto.AutoSize = true;
            this.label_cadastro_video_ponto.Location = new System.Drawing.Point(343, 0);
            this.label_cadastro_video_ponto.Name = "label_cadastro_video_ponto";
            this.label_cadastro_video_ponto.Size = new System.Drawing.Size(37, 13);
            this.label_cadastro_video_ponto.TabIndex = 7;
            this.label_cadastro_video_ponto.Text = "ponto:";
            // 
            // label_ponto
            // 
            this.label_ponto.AutoSize = true;
            this.label_ponto.Location = new System.Drawing.Point(343, 37);
            this.label_ponto.Name = "label_ponto";
            this.label_ponto.Size = new System.Drawing.Size(62, 13);
            this.label_ponto.TabIndex = 3;
            this.label_ponto.Text = "label_ponto";
            // 
            // label_cadastro_video_camera
            // 
            this.label_cadastro_video_camera.AutoSize = true;
            this.label_cadastro_video_camera.Location = new System.Drawing.Point(258, 0);
            this.label_cadastro_video_camera.Name = "label_cadastro_video_camera";
            this.label_cadastro_video_camera.Size = new System.Drawing.Size(45, 13);
            this.label_cadastro_video_camera.TabIndex = 6;
            this.label_cadastro_video_camera.Text = "camera:";
            // 
            // label_camera
            // 
            this.label_camera.AutoSize = true;
            this.label_camera.Location = new System.Drawing.Point(258, 37);
            this.label_camera.Name = "label_camera";
            this.label_camera.Size = new System.Drawing.Size(70, 13);
            this.label_camera.TabIndex = 2;
            this.label_camera.Text = "label_camera";
            // 
            // label_cadastro_vdeo_data_hora
            // 
            this.label_cadastro_vdeo_data_hora.AutoSize = true;
            this.label_cadastro_vdeo_data_hora.Location = new System.Drawing.Point(173, 0);
            this.label_cadastro_vdeo_data_hora.Name = "label_cadastro_vdeo_data_hora";
            this.label_cadastro_vdeo_data_hora.Size = new System.Drawing.Size(55, 26);
            this.label_cadastro_vdeo_data_hora.TabIndex = 5;
            this.label_cadastro_vdeo_data_hora.Text = "data hora inicio:";
            // 
            // label_hora_inicio
            // 
            this.label_hora_inicio.AutoSize = true;
            this.label_hora_inicio.Location = new System.Drawing.Point(173, 37);
            this.label_hora_inicio.Name = "label_hora_inicio";
            this.label_hora_inicio.Size = new System.Drawing.Size(78, 26);
            this.label_hora_inicio.TabIndex = 1;
            this.label_hora_inicio.Text = "label_hora_inicio";
            // 
            // label_cadastro_video_id
            // 
            this.label_cadastro_video_id.AutoSize = true;
            this.label_cadastro_video_id.Location = new System.Drawing.Point(88, 0);
            this.label_cadastro_video_id.Name = "label_cadastro_video_id";
            this.label_cadastro_video_id.Size = new System.Drawing.Size(18, 13);
            this.label_cadastro_video_id.TabIndex = 4;
            this.label_cadastro_video_id.Text = "id:";
            // 
            // label_cadastro_grupo_id
            // 
            this.label_cadastro_grupo_id.AutoSize = true;
            this.label_cadastro_grupo_id.Location = new System.Drawing.Point(3, 0);
            this.label_cadastro_grupo_id.Name = "label_cadastro_grupo_id";
            this.label_cadastro_grupo_id.Size = new System.Drawing.Size(50, 13);
            this.label_cadastro_grupo_id.TabIndex = 9;
            this.label_cadastro_grupo_id.Text = "Grupo id:";
            // 
            // label_cadastro_videdo_frente_tras
            // 
            this.label_cadastro_videdo_frente_tras.AutoSize = true;
            this.label_cadastro_videdo_frente_tras.Location = new System.Drawing.Point(428, 0);
            this.label_cadastro_videdo_frente_tras.Name = "label_cadastro_videdo_frente_tras";
            this.label_cadastro_videdo_frente_tras.Size = new System.Drawing.Size(60, 13);
            this.label_cadastro_videdo_frente_tras.TabIndex = 11;
            this.label_cadastro_videdo_frente_tras.Text = "frente_tras:";
            // 
            // label_grupo_id
            // 
            this.label_grupo_id.AutoSize = true;
            this.label_grupo_id.Location = new System.Drawing.Point(3, 37);
            this.label_grupo_id.Name = "label_grupo_id";
            this.label_grupo_id.Size = new System.Drawing.Size(48, 13);
            this.label_grupo_id.TabIndex = 10;
            this.label_grupo_id.Text = "grupo_id";
            // 
            // label_video_id
            // 
            this.label_video_id.AutoSize = true;
            this.label_video_id.Location = new System.Drawing.Point(88, 37);
            this.label_video_id.Name = "label_video_id";
            this.label_video_id.Size = new System.Drawing.Size(75, 13);
            this.label_video_id.TabIndex = 8;
            this.label_video_id.Text = "label_video_id";
            // 
            // label_frente_tras
            // 
            this.label_frente_tras.AutoSize = true;
            this.label_frente_tras.Location = new System.Drawing.Point(428, 37);
            this.label_frente_tras.Name = "label_frente_tras";
            this.label_frente_tras.Size = new System.Drawing.Size(60, 13);
            this.label_frente_tras.TabIndex = 12;
            this.label_frente_tras.Text = "frente_tras:";
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(3, 280);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(283, 40);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lista de grupos de video Cadastrados";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(3, 328);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(690, 152);
            this.dataGridView2.TabIndex = 11;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.buttonGerarImagens, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.buttonevento, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.buttonpublicar, 0, 2);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(699, 328);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(82, 152);
            this.tableLayoutPanel4.TabIndex = 13;
            // 
            // buttonGerarImagens
            // 
            this.buttonGerarImagens.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGerarImagens.Location = new System.Drawing.Point(3, 53);
            this.buttonGerarImagens.Name = "buttonGerarImagens";
            this.buttonGerarImagens.Size = new System.Drawing.Size(76, 44);
            this.buttonGerarImagens.TabIndex = 13;
            this.buttonGerarImagens.Text = "Gerar imagens";
            this.buttonGerarImagens.UseVisualStyleBackColor = true;
            this.buttonGerarImagens.Click += new System.EventHandler(this.buttonGerarImagens_Click);
            // 
            // buttonpublicar
            // 
            this.buttonpublicar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonpublicar.Location = new System.Drawing.Point(3, 103);
            this.buttonpublicar.Name = "buttonpublicar";
            this.buttonpublicar.Size = new System.Drawing.Size(76, 46);
            this.buttonpublicar.TabIndex = 14;
            this.buttonpublicar.Text = "Publicar";
            this.buttonpublicar.UseVisualStyleBackColor = true;
            this.buttonpublicar.Click += new System.EventHandler(this.buttonpublicar_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(88, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "ponto:";
            // 
            // ProjetoAbrirProjeto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 583);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ProjetoAbrirProjeto";
            this.Text = "Abrir Projeto";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonevento;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label_usuario;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label id;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label_projeto_id;
        private System.Windows.Forms.Label label_cadastro_video_id;
        private System.Windows.Forms.Label label_ponto;
        private System.Windows.Forms.Label label_cadastro_video_ponto;
        private System.Windows.Forms.Label label_projeto;
        private System.Windows.Forms.Label label_cadastro_video_camera;
        private System.Windows.Forms.Label label_camera;
        private System.Windows.Forms.Label label_cadastro_vdeo_data_hora;
        private System.Windows.Forms.Label label_hora_inicio;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label_video_id;
        private System.Windows.Forms.Label label_grupo_id;
        private System.Windows.Forms.Label label_cadastro_grupo_id;
        private System.Windows.Forms.Label label_cadastro_videdo_frente_tras;
        private System.Windows.Forms.Label label_frente_tras;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonpublicar;
        private System.Windows.Forms.Button buttonGerarImagens;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
    }
}