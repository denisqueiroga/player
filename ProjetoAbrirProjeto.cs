﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;


namespace WindowsFormsApplication1
{
    public partial class ProjetoAbrirProjeto : System.Windows.Forms.Form
    {
        //
        // Iniciacao do objeto
        //
        public ProjetoAbrirProjeto()
        {
            InitializeComponent();
 
            // aumenta o tamanho do painel 
            this.Width = Screen.PrimaryScreen.WorkingArea.Width;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.WindowState = FormWindowState.Maximized;
            label_usuario.Text = Usuario.Login;

            // carrega o dadaGridView1
            dataGridView1.DataSource = Load();
            
        }
        //
        //Funçoes de banco de dados
        //
        private BindingSource Load()
        {
            //MySqlConnection sqliteCon = new MySqlConnection(dbConnectionString);
            MySqlConnection connection;
            string server;
            string database;
            string userid;
            string password;


            // server = "192.168.1.106";
            server = Usuario.IPDB;
            database = "mydb";
            userid = "player";
            password = "player";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "USERID=" + userid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
            DataSet ds = new DataSet();
            BindingSource bs = new BindingSource();

            //open connection to database
            connection.Open();

            //select statament
            string Query = "select * from projeto";
            MySqlCommand cmd = new MySqlCommand(Query, connection);
            //Adapter
            // DataTable dataTable = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
            adapter.SelectCommand = cmd;
            adapter.Fill(ds);
            bs.DataSource = ds.Tables[0];
            return bs;
        }
        private BindingSource SubLoad(string projeto_id)
        {
            //MySqlConnection sqliteCon = new MySqlConnection(dbConnectionString);
            MySqlConnection connection;
            string server;
            string database;
            string userid;
            string password;


            // server = "192.168.1.106";
            server = Usuario.IPDB;
            database = "mydb";
            userid = "player";
            password = "player";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "USERID=" + userid + ";" + "PASSWORD=" + password + ";Allow Zero Datetime = true;";

            connection = new MySqlConnection(connectionString);
            DataSet ds = new DataSet();
            BindingSource bs = new BindingSource();

            //open connection to database
            connection.Open();

            //select statament
            string Query = "SELECT id, projeto_id, grupo_id, ponto, camera, `status`,arquivo, path, gps_latitude,gps_longitude,CAST(`hora_inicio` as char) as `hora_inicio`,CAST(`hora_final` as char) as `hora_final`, frente_tras"+
            " FROM view_cadastro_video WHERE `projeto_id` = " + projeto_id;
            MySqlCommand cmd = new MySqlCommand(Query, connection);
            //Adapter
            // DataTable dataTable = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
            adapter.SelectCommand = cmd;
            adapter.Fill(ds);
            bs.DataSource = ds.Tables[0];
            return bs;
        }
        //
        //Funçoes de ler o projeto do dataGridView e carregar o dataGridView2 com este parametro
        //
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex >= 0)&& (e.ColumnIndex >= 0))
            {
                //proibir edicao
                dataGridView1.Rows[e.RowIndex].ReadOnly = true;
                if (dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null)
                {
                    dataGridView1.Rows[e.RowIndex].ReadOnly = false;
                }
                // ler os dados no painel

                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                    label_projeto.Text = row.Cells["projeto"].Value.ToString();
                    //label_camera.Text = row.Cells["camera"].Value.ToString();
                    //label_hora_inicio.Text = row.Cells["hora_inicio"].Value.ToString();
                    //label_frente_traz.Text = row.Cells["frente_tras"].Value.ToString();
                    label_projeto_id.Text = row.Cells["id"].Value.ToString();




                    dataGridView2.DataSource = SubLoad(row.Cells["id"].Value.ToString());

                }

                // deixa em branco a selecao do segundo datagrid

                label_ponto.Text = "";
                label_camera.Text = "";
                label_hora_inicio.Text = "";
                label_ponto.Text = "";
                label_video_id.Text = "";
            }
        }
        //
        //Funçoes de ler o dataGridview2 que definel se o botoes vao funcionar
        //
        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex >= 0) && (e.ColumnIndex >= 0))
            {

                //proibir edicao
                dataGridView2.Rows[e.RowIndex].ReadOnly = true;
                if (dataGridView2.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null)
                {
                    dataGridView2.Rows[e.RowIndex].ReadOnly = false;
                }
                // ler os dados no painel

                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.dataGridView2.Rows[e.RowIndex];
                    label_ponto.Text = row.Cells["ponto"].Value.ToString();
                    label_camera.Text = row.Cells["camera"].Value.ToString();
                    label_hora_inicio.Text = row.Cells["hora_inicio"].Value.ToString();
                    label_frente_tras.Text = row.Cells["frente_tras"].Value.ToString();
                    label_video_id.Text = row.Cells["id"].Value.ToString();
                    label_grupo_id.Text = row.Cells["grupo_id"].Value.ToString();
                }
            }
        }
        //
        //abrir projeto selecionado para cada botao com a informacao do dataGridView2
        //
        private void buttonabrir_Click(object sender, EventArgs e)
        {
            if (label_video_id.Text != "")
            {

                // ler os dados no painel

                string projeto_id;
                string ponto;
                int cadastrovideo_id1;
                int cadastrogrupo_id1;

                try
                {
                    DataGridViewRow row = this.dataGridView2.Rows[dataGridView2.CurrentRow.Index];
                    //file1 = row.Cells["path"].Value.ToString() + "\\" + row.Cells["arquivo"].Value.ToString();
                    projeto_id = row.Cells["projeto_id"].Value.ToString();
                    ponto = row.Cells["ponto"].Value.ToString();
                    cadastrovideo_id1 = Convert.ToInt32(row.Cells["id"].Value.ToString());
                    cadastrogrupo_id1 = Convert.ToInt32(row.Cells["grupo_id"].Value.ToString());                //cadastro_video_id = label_projeto_id.Text;

                    //FecharFormulariosFilhos();
                    ProjetoEventos newMDIChild = new ProjetoEventos(cadastrogrupo_id1);

                    // Set the Parent Form of the Child window.
                    newMDIChild.MdiParent = this.ParentForm;
                    // Display the new form.

                    newMDIChild.Show();
                }
                catch
                {
                    MessageBox.Show("Selecione um item na tabela de cadastro de video");
                }
            }
            else
            {

                MessageBox.Show("Selecionar um video");
            }
        }
        private void buttonpublicar_Click(object sender, EventArgs e)
        {
            if (label_video_id.Text != "")
            {

                // ler os dados no painel

                string projeto_id;
                string ponto;
                int cadastrovideo_id1;
                int cadastrogrupo_id1;

                DataGridViewRow row = this.dataGridView2.Rows[dataGridView2.CurrentRow.Index];
                //file1 = row.Cells["path"].Value.ToString() + "\\" + row.Cells["arquivo"].Value.ToString();
                projeto_id = row.Cells["projeto_id"].Value.ToString();
                ponto = row.Cells["ponto"].Value.ToString();
                cadastrovideo_id1 = Convert.ToInt32(row.Cells["id"].Value.ToString());
                cadastrogrupo_id1 = Convert.ToInt32(row.Cells["grupo_id"].Value.ToString());                //cadastro_video_id = label_projeto_id.Text;

                //FecharFormulariosFilhos();
                ProjetoEventoPublicar newMDIChild = new ProjetoEventoPublicar(cadastrogrupo_id1);

                // Set the Parent Form of the Child window.
                newMDIChild.MdiParent = this.ParentForm;
                // Display the new form.

                newMDIChild.Show();

            }
            else
            {

                MessageBox.Show("Selecionar um video");
            }
        }
        private void buttonGerarImagens_Click(object sender, EventArgs e)
        {
            if (label_video_id.Text != "")
            {

                // ler os dados no painel

                string projeto_id;
                string ponto;
                int cadastrovideo_id1;
                int cadastrogrupo_id1;

                DataGridViewRow row = this.dataGridView2.Rows[dataGridView2.CurrentRow.Index];
                //file1 = row.Cells["path"].Value.ToString() + "\\" + row.Cells["arquivo"].Value.ToString();
                projeto_id = row.Cells["projeto_id"].Value.ToString();
                ponto = row.Cells["ponto"].Value.ToString();
                cadastrovideo_id1 = Convert.ToInt32(row.Cells["id"].Value.ToString());
                cadastrogrupo_id1 = Convert.ToInt32(row.Cells["grupo_id"].Value.ToString());                //cadastro_video_id = label_projeto_id.Text;

                //FecharFormulariosFilhos();
                ProjetoEventoFotoMain newMDIChild = new ProjetoEventoFotoMain(cadastrogrupo_id1);

                // Set the Parent Form of the Child window.
                newMDIChild.MdiParent = this.ParentForm;
                // Display the new form.

                newMDIChild.Show();

            }
            else
            {

                MessageBox.Show("Selecionar um video");
            }
        }
        //
        //Funçoes fechar todas as sub janelas
        //
        private void FecharFormulariosFilhos()
        {
            // percorre todos os formulários abertos
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                // se o formulário for filho
                if (Application.OpenForms[i].IsMdiChild)
                {
                    // fecha o formulário
                    Application.OpenForms[i].Close();
                }
            }
        }
    }
}





