﻿namespace WindowsFormsApplication1
{
    partial class Status
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label_usuario = new System.Windows.Forms.Label();
            this.label_senha = new System.Windows.Forms.Label();
            this.label_Perfil = new System.Windows.Forms.Label();
            this.label_Islogin = new System.Windows.Forms.Label();
            this.label_IPDB = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Usuario";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Senha";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Perfil";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 139);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Islogin";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 171);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "IPDB";
            // 
            // label_usuario
            // 
            this.label_usuario.AutoSize = true;
            this.label_usuario.Location = new System.Drawing.Point(117, 24);
            this.label_usuario.Name = "label_usuario";
            this.label_usuario.Size = new System.Drawing.Size(43, 13);
            this.label_usuario.TabIndex = 5;
            this.label_usuario.Text = "Usuario";
            // 
            // label_senha
            // 
            this.label_senha.AutoSize = true;
            this.label_senha.Location = new System.Drawing.Point(117, 60);
            this.label_senha.Name = "label_senha";
            this.label_senha.Size = new System.Drawing.Size(38, 13);
            this.label_senha.TabIndex = 6;
            this.label_senha.Text = "Senha";
            // 
            // label_Perfil
            // 
            this.label_Perfil.AutoSize = true;
            this.label_Perfil.Location = new System.Drawing.Point(117, 101);
            this.label_Perfil.Name = "label_Perfil";
            this.label_Perfil.Size = new System.Drawing.Size(30, 13);
            this.label_Perfil.TabIndex = 7;
            this.label_Perfil.Text = "Perfil";
            // 
            // label_Islogin
            // 
            this.label_Islogin.AutoSize = true;
            this.label_Islogin.Location = new System.Drawing.Point(117, 139);
            this.label_Islogin.Name = "label_Islogin";
            this.label_Islogin.Size = new System.Drawing.Size(37, 13);
            this.label_Islogin.TabIndex = 8;
            this.label_Islogin.Text = "Islogin";
            // 
            // label_IPDB
            // 
            this.label_IPDB.AutoSize = true;
            this.label_IPDB.Location = new System.Drawing.Point(117, 171);
            this.label_IPDB.Name = "label_IPDB";
            this.label_IPDB.Size = new System.Drawing.Size(32, 13);
            this.label_IPDB.TabIndex = 9;
            this.label_IPDB.Text = "IPDB";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Status
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(208, 211);
            this.Controls.Add(this.label_IPDB);
            this.Controls.Add(this.label_Islogin);
            this.Controls.Add(this.label_Perfil);
            this.Controls.Add(this.label_senha);
            this.Controls.Add(this.label_usuario);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Status";
            this.Text = "Sobre";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label_usuario;
        private System.Windows.Forms.Label label_senha;
        private System.Windows.Forms.Label label_Perfil;
        private System.Windows.Forms.Label label_Islogin;
        private System.Windows.Forms.Label label_IPDB;
        private System.Windows.Forms.Timer timer1;
    }
}