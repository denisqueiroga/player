﻿namespace WindowsFormsApplication1
{
    partial class ConfiguracaoCadastroUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.Novo = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.buttonLimpar = new System.Windows.Forms.Button();
            this.buttonSalvar = new System.Windows.Forms.Button();
            this.comboBoxGrupo = new System.Windows.Forms.ComboBox();
            this.labelNovoGrupo = new System.Windows.Forms.Label();
            this.textboxSenha = new System.Windows.Forms.TextBox();
            this.LabelSenha = new System.Windows.Forms.Label();
            this.LabelNome = new System.Windows.Forms.Label();
            this.textboxNome = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonApagar = new System.Windows.Forms.Button();
            this.labelApagarPerfil = new System.Windows.Forms.Label();
            this.ApagarPerfil = new System.Windows.Forms.TextBox();
            this.ApagarSenha = new System.Windows.Forms.TextBox();
            this.ApagarNome = new System.Windows.Forms.TextBox();
            this.labelApagarSenha = new System.Windows.Forms.Label();
            this.labelApagarNome = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.labelSenhaPerfil = new System.Windows.Forms.Label();
            this.TrocarPerfil = new System.Windows.Forms.TextBox();
            this.TrocarButton = new System.Windows.Forms.Button();
            this.validacaosenha = new System.Windows.Forms.Label();
            this.TrocarSenha2 = new System.Windows.Forms.TextBox();
            this.labelSenhaSenha2 = new System.Windows.Forms.Label();
            this.TrocarSenha1 = new System.Windows.Forms.TextBox();
            this.labelSenhaSenha1 = new System.Windows.Forms.Label();
            this.TrocarNome = new System.Windows.Forms.TextBox();
            this.labelSenhaNome = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.Novo.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Novo);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGridView1);
            this.splitContainer1.Size = new System.Drawing.Size(1246, 569);
            this.splitContainer1.SplitterDistance = 289;
            this.splitContainer1.TabIndex = 3;
            // 
            // Novo
            // 
            this.Novo.Controls.Add(this.tabPage1);
            this.Novo.Controls.Add(this.tabPage2);
            this.Novo.Controls.Add(this.tabPage3);
            this.Novo.Location = new System.Drawing.Point(12, 24);
            this.Novo.Name = "Novo";
            this.Novo.SelectedIndex = 0;
            this.Novo.Size = new System.Drawing.Size(274, 280);
            this.Novo.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.buttonLimpar);
            this.tabPage1.Controls.Add(this.buttonSalvar);
            this.tabPage1.Controls.Add(this.comboBoxGrupo);
            this.tabPage1.Controls.Add(this.labelNovoGrupo);
            this.tabPage1.Controls.Add(this.textboxSenha);
            this.tabPage1.Controls.Add(this.LabelSenha);
            this.tabPage1.Controls.Add(this.LabelNome);
            this.tabPage1.Controls.Add(this.textboxNome);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(266, 254);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Novo";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // buttonLimpar
            // 
            this.buttonLimpar.Location = new System.Drawing.Point(20, 165);
            this.buttonLimpar.Name = "buttonLimpar";
            this.buttonLimpar.Size = new System.Drawing.Size(75, 23);
            this.buttonLimpar.TabIndex = 7;
            this.buttonLimpar.Text = "Limpar";
            this.buttonLimpar.UseVisualStyleBackColor = true;
            this.buttonLimpar.Click += new System.EventHandler(this.buttonLimpar_Click);
            // 
            // buttonSalvar
            // 
            this.buttonSalvar.Location = new System.Drawing.Point(130, 165);
            this.buttonSalvar.Name = "buttonSalvar";
            this.buttonSalvar.Size = new System.Drawing.Size(75, 23);
            this.buttonSalvar.TabIndex = 6;
            this.buttonSalvar.Text = "Salvar";
            this.buttonSalvar.UseVisualStyleBackColor = true;
            this.buttonSalvar.Click += new System.EventHandler(this.buttonSalvar_Click);
            // 
            // comboBoxGrupo
            // 
            this.comboBoxGrupo.FormattingEnabled = true;
            this.comboBoxGrupo.Items.AddRange(new object[] {
            "Operador",
            "Administrador"});
            this.comboBoxGrupo.Location = new System.Drawing.Point(105, 103);
            this.comboBoxGrupo.Name = "comboBoxGrupo";
            this.comboBoxGrupo.Size = new System.Drawing.Size(100, 21);
            this.comboBoxGrupo.TabIndex = 5;
            this.comboBoxGrupo.Text = "Operador";
            // 
            // labelNovoGrupo
            // 
            this.labelNovoGrupo.AutoSize = true;
            this.labelNovoGrupo.Location = new System.Drawing.Point(8, 106);
            this.labelNovoGrupo.Name = "labelNovoGrupo";
            this.labelNovoGrupo.Size = new System.Drawing.Size(36, 13);
            this.labelNovoGrupo.TabIndex = 4;
            this.labelNovoGrupo.Text = "Grupo";
            // 
            // textboxSenha
            // 
            this.textboxSenha.Location = new System.Drawing.Point(105, 62);
            this.textboxSenha.Name = "textboxSenha";
            this.textboxSenha.Size = new System.Drawing.Size(100, 20);
            this.textboxSenha.TabIndex = 3;
            this.textboxSenha.Text = "Senha";
            // 
            // LabelSenha
            // 
            this.LabelSenha.AutoSize = true;
            this.LabelSenha.Location = new System.Drawing.Point(8, 65);
            this.LabelSenha.Name = "LabelSenha";
            this.LabelSenha.Size = new System.Drawing.Size(38, 13);
            this.LabelSenha.TabIndex = 2;
            this.LabelSenha.Text = "Senha";
            // 
            // LabelNome
            // 
            this.LabelNome.AutoSize = true;
            this.LabelNome.Location = new System.Drawing.Point(8, 28);
            this.LabelNome.Name = "LabelNome";
            this.LabelNome.Size = new System.Drawing.Size(35, 13);
            this.LabelNome.TabIndex = 1;
            this.LabelNome.Text = "Nome";
            // 
            // textboxNome
            // 
            this.textboxNome.Location = new System.Drawing.Point(105, 25);
            this.textboxNome.Name = "textboxNome";
            this.textboxNome.Size = new System.Drawing.Size(100, 20);
            this.textboxNome.TabIndex = 0;
            this.textboxNome.Text = "Nome";
            this.textboxNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textboxNome_KeyPress);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.buttonApagar);
            this.tabPage2.Controls.Add(this.labelApagarPerfil);
            this.tabPage2.Controls.Add(this.ApagarPerfil);
            this.tabPage2.Controls.Add(this.ApagarSenha);
            this.tabPage2.Controls.Add(this.ApagarNome);
            this.tabPage2.Controls.Add(this.labelApagarSenha);
            this.tabPage2.Controls.Add(this.labelApagarNome);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(266, 254);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Apagar";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // buttonApagar
            // 
            this.buttonApagar.Location = new System.Drawing.Point(130, 165);
            this.buttonApagar.Name = "buttonApagar";
            this.buttonApagar.Size = new System.Drawing.Size(75, 23);
            this.buttonApagar.TabIndex = 6;
            this.buttonApagar.Text = "Apagar";
            this.buttonApagar.UseVisualStyleBackColor = true;
            this.buttonApagar.Click += new System.EventHandler(this.buttonApgar_Click);
            // 
            // labelApagarPerfil
            // 
            this.labelApagarPerfil.AutoSize = true;
            this.labelApagarPerfil.Location = new System.Drawing.Point(7, 106);
            this.labelApagarPerfil.Name = "labelApagarPerfil";
            this.labelApagarPerfil.Size = new System.Drawing.Size(30, 13);
            this.labelApagarPerfil.TabIndex = 5;
            this.labelApagarPerfil.Text = "Perfil";
            // 
            // ApagarPerfil
            // 
            this.ApagarPerfil.Location = new System.Drawing.Point(105, 103);
            this.ApagarPerfil.Name = "ApagarPerfil";
            this.ApagarPerfil.Size = new System.Drawing.Size(100, 20);
            this.ApagarPerfil.TabIndex = 4;
            // 
            // ApagarSenha
            // 
            this.ApagarSenha.Location = new System.Drawing.Point(105, 62);
            this.ApagarSenha.Name = "ApagarSenha";
            this.ApagarSenha.Size = new System.Drawing.Size(100, 20);
            this.ApagarSenha.TabIndex = 3;
            // 
            // ApagarNome
            // 
            this.ApagarNome.Location = new System.Drawing.Point(105, 25);
            this.ApagarNome.Name = "ApagarNome";
            this.ApagarNome.Size = new System.Drawing.Size(100, 20);
            this.ApagarNome.TabIndex = 2;
            // 
            // labelApagarSenha
            // 
            this.labelApagarSenha.AutoSize = true;
            this.labelApagarSenha.Location = new System.Drawing.Point(7, 65);
            this.labelApagarSenha.Name = "labelApagarSenha";
            this.labelApagarSenha.Size = new System.Drawing.Size(38, 13);
            this.labelApagarSenha.TabIndex = 1;
            this.labelApagarSenha.Text = "Senha";
            // 
            // labelApagarNome
            // 
            this.labelApagarNome.AutoSize = true;
            this.labelApagarNome.Location = new System.Drawing.Point(6, 28);
            this.labelApagarNome.Name = "labelApagarNome";
            this.labelApagarNome.Size = new System.Drawing.Size(35, 13);
            this.labelApagarNome.TabIndex = 0;
            this.labelApagarNome.Text = "Nome";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.labelSenhaPerfil);
            this.tabPage3.Controls.Add(this.TrocarPerfil);
            this.tabPage3.Controls.Add(this.TrocarButton);
            this.tabPage3.Controls.Add(this.validacaosenha);
            this.tabPage3.Controls.Add(this.TrocarSenha2);
            this.tabPage3.Controls.Add(this.labelSenhaSenha2);
            this.tabPage3.Controls.Add(this.TrocarSenha1);
            this.tabPage3.Controls.Add(this.labelSenhaSenha1);
            this.tabPage3.Controls.Add(this.TrocarNome);
            this.tabPage3.Controls.Add(this.labelSenhaNome);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(266, 254);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Senha";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // labelSenhaPerfil
            // 
            this.labelSenhaPerfil.AutoSize = true;
            this.labelSenhaPerfil.Location = new System.Drawing.Point(19, 62);
            this.labelSenhaPerfil.Name = "labelSenhaPerfil";
            this.labelSenhaPerfil.Size = new System.Drawing.Size(30, 13);
            this.labelSenhaPerfil.TabIndex = 9;
            this.labelSenhaPerfil.Text = "Perfil";
            // 
            // TrocarPerfil
            // 
            this.TrocarPerfil.Location = new System.Drawing.Point(105, 57);
            this.TrocarPerfil.Name = "TrocarPerfil";
            this.TrocarPerfil.Size = new System.Drawing.Size(100, 20);
            this.TrocarPerfil.TabIndex = 8;
            // 
            // TrocarButton
            // 
            this.TrocarButton.Location = new System.Drawing.Point(130, 165);
            this.TrocarButton.Name = "TrocarButton";
            this.TrocarButton.Size = new System.Drawing.Size(75, 23);
            this.TrocarButton.TabIndex = 7;
            this.TrocarButton.Text = "Trocar";
            this.TrocarButton.UseVisualStyleBackColor = true;
            this.TrocarButton.Click += new System.EventHandler(this.TrocarButton_Click_2);
            // 
            // validacaosenha
            // 
            this.validacaosenha.AutoSize = true;
            this.validacaosenha.Location = new System.Drawing.Point(76, 200);
            this.validacaosenha.Name = "validacaosenha";
            this.validacaosenha.Size = new System.Drawing.Size(16, 13);
            this.validacaosenha.TabIndex = 6;
            this.validacaosenha.Text = "...";
            // 
            // TrocarSenha2
            // 
            this.TrocarSenha2.Location = new System.Drawing.Point(105, 113);
            this.TrocarSenha2.Name = "TrocarSenha2";
            this.TrocarSenha2.Size = new System.Drawing.Size(100, 20);
            this.TrocarSenha2.TabIndex = 5;
            this.TrocarSenha2.TextChanged += new System.EventHandler(this.TrocarSenha2_TextChanged);
            // 
            // labelSenhaSenha2
            // 
            this.labelSenhaSenha2.AutoSize = true;
            this.labelSenhaSenha2.Location = new System.Drawing.Point(19, 116);
            this.labelSenhaSenha2.Name = "labelSenhaSenha2";
            this.labelSenhaSenha2.Size = new System.Drawing.Size(36, 13);
            this.labelSenhaSenha2.TabIndex = 4;
            this.labelSenhaSenha2.Text = "senha";
            // 
            // TrocarSenha1
            // 
            this.TrocarSenha1.Location = new System.Drawing.Point(105, 83);
            this.TrocarSenha1.Name = "TrocarSenha1";
            this.TrocarSenha1.Size = new System.Drawing.Size(100, 20);
            this.TrocarSenha1.TabIndex = 3;
            this.TrocarSenha1.TextChanged += new System.EventHandler(this.TrocarSenha1_TextChanged);
            // 
            // labelSenhaSenha1
            // 
            this.labelSenhaSenha1.AutoSize = true;
            this.labelSenhaSenha1.Location = new System.Drawing.Point(19, 86);
            this.labelSenhaSenha1.Name = "labelSenhaSenha1";
            this.labelSenhaSenha1.Size = new System.Drawing.Size(36, 13);
            this.labelSenhaSenha1.TabIndex = 2;
            this.labelSenhaSenha1.Text = "senha";
            // 
            // TrocarNome
            // 
            this.TrocarNome.Location = new System.Drawing.Point(105, 25);
            this.TrocarNome.Name = "TrocarNome";
            this.TrocarNome.Size = new System.Drawing.Size(100, 20);
            this.TrocarNome.TabIndex = 1;
            // 
            // labelSenhaNome
            // 
            this.labelSenhaNome.AutoSize = true;
            this.labelSenhaNome.Location = new System.Drawing.Point(19, 34);
            this.labelSenhaNome.Name = "labelSenhaNome";
            this.labelSenhaNome.Size = new System.Drawing.Size(35, 13);
            this.labelSenhaNome.TabIndex = 0;
            this.labelSenhaNome.Text = "Nome";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(3, 24);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(950, 542);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            this.dataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
            // 
            // ConfiguracaoCadastroUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1244, 572);
            this.Controls.Add(this.splitContainer1);
            this.Name = "ConfiguracaoCadastroUsuario";
            this.Text = "Cadastro de Usuário";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.Novo.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabControl Novo;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox comboBoxGrupo;
        private System.Windows.Forms.Label labelNovoGrupo;
        private System.Windows.Forms.TextBox textboxSenha;
        private System.Windows.Forms.Label LabelSenha;
        private System.Windows.Forms.Label LabelNome;
        private System.Windows.Forms.TextBox textboxNome;
        private System.Windows.Forms.Button buttonLimpar;
        private System.Windows.Forms.Button buttonSalvar;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button buttonApagar;
        private System.Windows.Forms.Label labelApagarPerfil;
        private System.Windows.Forms.TextBox ApagarPerfil;
        private System.Windows.Forms.TextBox ApagarSenha;
        private System.Windows.Forms.TextBox ApagarNome;
        private System.Windows.Forms.Label labelApagarSenha;
        private System.Windows.Forms.Label labelApagarNome;
        private System.Windows.Forms.Label validacaosenha;
        private System.Windows.Forms.TextBox TrocarSenha2;
        private System.Windows.Forms.Label labelSenhaSenha2;
        private System.Windows.Forms.TextBox TrocarSenha1;
        private System.Windows.Forms.Label labelSenhaSenha1;
        private System.Windows.Forms.TextBox TrocarNome;
        private System.Windows.Forms.Label labelSenhaNome;
        private System.Windows.Forms.Button TrocarButton;
        private System.Windows.Forms.Label labelSenhaPerfil;
        private System.Windows.Forms.TextBox TrocarPerfil;
    }
}