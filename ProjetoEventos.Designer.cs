﻿namespace WindowsFormsApplication1
{
    partial class ProjetoEventos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjetoEventos));
            this.buttonSincronizarVideo = new System.Windows.Forms.Button();
            this.currentPositionStringA = new System.Windows.Forms.Label();
            this.timercontador = new System.Windows.Forms.Timer(this.components);
            this.timerbackforward = new System.Windows.Forms.Timer(this.components);
            this.vidoe1 = new System.Windows.Forms.Button();
            this.buttonvideo2 = new System.Windows.Forms.Button();
            this.videoB = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.labelcurrentPositionA = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelEixoCampo = new System.Windows.Forms.Label();
            this.labelVeiculoCampo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelcurrentPositionB = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelVeiculo = new System.Windows.Forms.Label();
            this.buttonEvento = new System.Windows.Forms.Button();
            this.tableLayoutPanelVideo = new System.Windows.Forms.TableLayoutPanel();
            this.axWindowsMediaPlayer2 = new AxWMPLib.AxWindowsMediaPlayer();
            this.axWindowsMediaPlayer1 = new AxWMPLib.AxWindowsMediaPlayer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanelClassificacao1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelVeiculo1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelVeiculo2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelVeiculo3 = new System.Windows.Forms.TableLayoutPanel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanelClassificacao2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelEixo1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelControleEventos = new System.Windows.Forms.TableLayoutPanel();
            this.labeleventoid = new System.Windows.Forms.Label();
            this.buttonVer = new System.Windows.Forms.Button();
            this.buttonApagar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.mySqlConnectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Controles = new System.Windows.Forms.GroupBox();
            this.labelPonto = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelNomeProjeto = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.currentPositionStringB = new System.Windows.Forms.Label();
            this.buttonMover = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.Velocidade2 = new System.Windows.Forms.Label();
            this.Velocidade1 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.buttonPlayBackward = new System.Windows.Forms.Button();
            this.buttonPlayForward = new System.Windows.Forms.Button();
            this.buttonPause = new System.Windows.Forms.Button();
            this.buttonPlay = new System.Windows.Forms.Button();
            this.tableLayoutPanelGeral = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDowndelay = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonSalvarImagem = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.checkBox_exapandir = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanelVideo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanelClassificacao1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanelClassificacao2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanelControleEventos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mySqlConnectionBindingSource)).BeginInit();
            this.Controles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.tableLayoutPanelGeral.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDowndelay)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonSincronizarVideo
            // 
            this.buttonSincronizarVideo.Location = new System.Drawing.Point(138, 9);
            this.buttonSincronizarVideo.Name = "buttonSincronizarVideo";
            this.buttonSincronizarVideo.Size = new System.Drawing.Size(107, 23);
            this.buttonSincronizarVideo.TabIndex = 8;
            this.buttonSincronizarVideo.Text = "Sincronizar video";
            this.buttonSincronizarVideo.UseVisualStyleBackColor = true;
            this.buttonSincronizarVideo.Click += new System.EventHandler(this.buttonSincronizarVideo_Click);
            // 
            // currentPositionStringA
            // 
            this.currentPositionStringA.AutoSize = true;
            this.currentPositionStringA.Location = new System.Drawing.Point(304, 16);
            this.currentPositionStringA.Name = "currentPositionStringA";
            this.currentPositionStringA.Size = new System.Drawing.Size(26, 13);
            this.currentPositionStringA.TabIndex = 9;
            this.currentPositionStringA.Text = "time";
            // 
            // timercontador
            // 
            this.timercontador.Enabled = true;
            this.timercontador.Tick += new System.EventHandler(this.timercontador_Tick);
            // 
            // timerbackforward
            // 
            this.timerbackforward.Tick += new System.EventHandler(this.timerbackforward_Tick);
            // 
            // vidoe1
            // 
            this.vidoe1.Location = new System.Drawing.Point(67, 12);
            this.vidoe1.Name = "vidoe1";
            this.vidoe1.Size = new System.Drawing.Size(55, 23);
            this.vidoe1.TabIndex = 30;
            this.vidoe1.Text = " video A";
            this.vidoe1.UseVisualStyleBackColor = true;
            this.vidoe1.Click += new System.EventHandler(this.buttonvideoA_Click);
            // 
            // buttonvideo2
            // 
            this.buttonvideo2.Location = new System.Drawing.Point(6, 12);
            this.buttonvideo2.Name = "buttonvideo2";
            this.buttonvideo2.Size = new System.Drawing.Size(55, 23);
            this.buttonvideo2.TabIndex = 32;
            this.buttonvideo2.Text = "2 video";
            this.buttonvideo2.UseVisualStyleBackColor = true;
            this.buttonvideo2.Click += new System.EventHandler(this.buttonvideo2_Click);
            // 
            // videoB
            // 
            this.videoB.Location = new System.Drawing.Point(127, 12);
            this.videoB.Name = "videoB";
            this.videoB.Size = new System.Drawing.Size(55, 23);
            this.videoB.TabIndex = 33;
            this.videoB.Text = " video B";
            this.videoB.UseVisualStyleBackColor = true;
            this.videoB.Click += new System.EventHandler(this.buttonvideoB_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(224, 9);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(63, 23);
            this.button4.TabIndex = 33;
            this.button4.Text = " video B";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel2.Controls.Add(this.labelcurrentPositionA, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.labelEixoCampo, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.labelVeiculoCampo, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelcurrentPositionB, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label8, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.labelVeiculo, 0, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(467, 383);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(155, 101);
            this.tableLayoutPanel2.TabIndex = 34;
            // 
            // labelcurrentPositionA
            // 
            this.labelcurrentPositionA.AutoSize = true;
            this.labelcurrentPositionA.Location = new System.Drawing.Point(57, 0);
            this.labelcurrentPositionA.Name = "labelcurrentPositionA";
            this.labelcurrentPositionA.Size = new System.Drawing.Size(33, 13);
            this.labelcurrentPositionA.TabIndex = 10;
            this.labelcurrentPositionA.Text = "timeA";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 21);
            this.label7.TabIndex = 14;
            this.label7.Text = "Classificacao 2";
            // 
            // labelEixoCampo
            // 
            this.labelEixoCampo.AutoSize = true;
            this.labelEixoCampo.Location = new System.Drawing.Point(57, 80);
            this.labelEixoCampo.Name = "labelEixoCampo";
            this.labelEixoCampo.Size = new System.Drawing.Size(0, 13);
            this.labelEixoCampo.TabIndex = 16;
            // 
            // labelVeiculoCampo
            // 
            this.labelVeiculoCampo.AutoSize = true;
            this.labelVeiculoCampo.Location = new System.Drawing.Point(57, 60);
            this.labelVeiculoCampo.Name = "labelVeiculoCampo";
            this.labelVeiculoCampo.Size = new System.Drawing.Size(0, 13);
            this.labelVeiculoCampo.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "time A";
            // 
            // labelcurrentPositionB
            // 
            this.labelcurrentPositionB.AutoSize = true;
            this.labelcurrentPositionB.Location = new System.Drawing.Point(57, 30);
            this.labelcurrentPositionB.Name = "labelcurrentPositionB";
            this.labelcurrentPositionB.Size = new System.Drawing.Size(33, 13);
            this.labelcurrentPositionB.TabIndex = 18;
            this.labelcurrentPositionB.Text = "timeB";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 20);
            this.label8.TabIndex = 17;
            this.label8.Text = "Classificacao 1";
            // 
            // labelVeiculo
            // 
            this.labelVeiculo.AutoSize = true;
            this.labelVeiculo.Location = new System.Drawing.Point(3, 30);
            this.labelVeiculo.Name = "labelVeiculo";
            this.labelVeiculo.Size = new System.Drawing.Size(36, 13);
            this.labelVeiculo.TabIndex = 13;
            this.labelVeiculo.Text = "time B";
            // 
            // buttonEvento
            // 
            this.buttonEvento.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEvento.Image = global::WindowsFormsApplication1.Properties.Resources.time1;
            this.buttonEvento.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.buttonEvento.Location = new System.Drawing.Point(628, 383);
            this.buttonEvento.Name = "buttonEvento";
            this.buttonEvento.Size = new System.Drawing.Size(248, 101);
            this.buttonEvento.TabIndex = 11;
            this.buttonEvento.Text = "Evento";
            this.buttonEvento.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonEvento.UseVisualStyleBackColor = true;
            this.buttonEvento.Click += new System.EventHandler(this.buttonEvento_Click);
            // 
            // tableLayoutPanelVideo
            // 
            this.tableLayoutPanelVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelVideo.ColumnCount = 2;
            this.tableLayoutPanelVideo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelVideo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelVideo.Controls.Add(this.axWindowsMediaPlayer2, 1, 0);
            this.tableLayoutPanelVideo.Controls.Add(this.axWindowsMediaPlayer1, 0, 0);
            this.tableLayoutPanelVideo.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelVideo.Name = "tableLayoutPanelVideo";
            this.tableLayoutPanelVideo.RowCount = 1;
            this.tableLayoutPanelVideo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelVideo.Size = new System.Drawing.Size(458, 374);
            this.tableLayoutPanelVideo.TabIndex = 33;
            // 
            // axWindowsMediaPlayer2
            // 
            this.axWindowsMediaPlayer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.axWindowsMediaPlayer2.Enabled = true;
            this.axWindowsMediaPlayer2.Location = new System.Drawing.Point(232, 3);
            this.axWindowsMediaPlayer2.Name = "axWindowsMediaPlayer2";
            this.axWindowsMediaPlayer2.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axWindowsMediaPlayer2.OcxState")));
            this.axWindowsMediaPlayer2.Size = new System.Drawing.Size(223, 368);
            this.axWindowsMediaPlayer2.TabIndex = 7;
            this.axWindowsMediaPlayer2.PositionChange += new AxWMPLib._WMPOCXEvents_PositionChangeEventHandler(this.axWindowsMediaPlayer2_PositionChange);
            // 
            // axWindowsMediaPlayer1
            // 
            this.axWindowsMediaPlayer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.axWindowsMediaPlayer1.Enabled = true;
            this.axWindowsMediaPlayer1.Location = new System.Drawing.Point(3, 3);
            this.axWindowsMediaPlayer1.Name = "axWindowsMediaPlayer1";
            this.axWindowsMediaPlayer1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axWindowsMediaPlayer1.OcxState")));
            this.axWindowsMediaPlayer1.Size = new System.Drawing.Size(223, 368);
            this.axWindowsMediaPlayer1.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(467, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(155, 374);
            this.tabControl1.TabIndex = 32;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanelClassificacao1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(147, 348);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Tipos e Eixos";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanelClassificacao1
            // 
            this.tableLayoutPanelClassificacao1.AutoSize = true;
            this.tableLayoutPanelClassificacao1.ColumnCount = 1;
            this.tableLayoutPanelClassificacao1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelClassificacao1.Controls.Add(this.tableLayoutPanelVeiculo1, 0, 0);
            this.tableLayoutPanelClassificacao1.Controls.Add(this.tableLayoutPanelVeiculo2, 0, 1);
            this.tableLayoutPanelClassificacao1.Controls.Add(this.tableLayoutPanelVeiculo3, 0, 2);
            this.tableLayoutPanelClassificacao1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelClassificacao1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelClassificacao1.Name = "tableLayoutPanelClassificacao1";
            this.tableLayoutPanelClassificacao1.RowCount = 3;
            this.tableLayoutPanelClassificacao1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanelClassificacao1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanelClassificacao1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.34F));
            this.tableLayoutPanelClassificacao1.Size = new System.Drawing.Size(141, 342);
            this.tableLayoutPanelClassificacao1.TabIndex = 32;
            // 
            // tableLayoutPanelVeiculo1
            // 
            this.tableLayoutPanelVeiculo1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelVeiculo1.AutoScroll = true;
            this.tableLayoutPanelVeiculo1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanelVeiculo1.ColumnCount = 2;
            this.tableLayoutPanelVeiculo1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelVeiculo1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelVeiculo1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelVeiculo1.Name = "tableLayoutPanelVeiculo1";
            this.tableLayoutPanelVeiculo1.RowCount = 2;
            this.tableLayoutPanelVeiculo1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelVeiculo1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelVeiculo1.Size = new System.Drawing.Size(200, 107);
            this.tableLayoutPanelVeiculo1.TabIndex = 0;
            // 
            // tableLayoutPanelVeiculo2
            // 
            this.tableLayoutPanelVeiculo2.AutoScroll = true;
            this.tableLayoutPanelVeiculo2.ColumnCount = 2;
            this.tableLayoutPanelVeiculo2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelVeiculo2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelVeiculo2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelVeiculo2.Location = new System.Drawing.Point(3, 116);
            this.tableLayoutPanelVeiculo2.Name = "tableLayoutPanelVeiculo2";
            this.tableLayoutPanelVeiculo2.RowCount = 2;
            this.tableLayoutPanelVeiculo2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelVeiculo2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelVeiculo2.Size = new System.Drawing.Size(200, 107);
            this.tableLayoutPanelVeiculo2.TabIndex = 1;
            // 
            // tableLayoutPanelVeiculo3
            // 
            this.tableLayoutPanelVeiculo3.AutoScroll = true;
            this.tableLayoutPanelVeiculo3.ColumnCount = 2;
            this.tableLayoutPanelVeiculo3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelVeiculo3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelVeiculo3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelVeiculo3.Location = new System.Drawing.Point(3, 229);
            this.tableLayoutPanelVeiculo3.Name = "tableLayoutPanelVeiculo3";
            this.tableLayoutPanelVeiculo3.RowCount = 2;
            this.tableLayoutPanelVeiculo3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelVeiculo3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelVeiculo3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelVeiculo3.Size = new System.Drawing.Size(200, 110);
            this.tableLayoutPanelVeiculo3.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanelClassificacao2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(147, 348);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Eixos no Solo";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanelClassificacao2
            // 
            this.tableLayoutPanelClassificacao2.ColumnCount = 1;
            this.tableLayoutPanelClassificacao2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelClassificacao2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelClassificacao2.Controls.Add(this.tableLayoutPanelEixo1, 0, 0);
            this.tableLayoutPanelClassificacao2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelClassificacao2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelClassificacao2.Name = "tableLayoutPanelClassificacao2";
            this.tableLayoutPanelClassificacao2.RowCount = 1;
            this.tableLayoutPanelClassificacao2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelClassificacao2.Size = new System.Drawing.Size(141, 342);
            this.tableLayoutPanelClassificacao2.TabIndex = 0;
            // 
            // tableLayoutPanelEixo1
            // 
            this.tableLayoutPanelEixo1.ColumnCount = 2;
            this.tableLayoutPanelEixo1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelEixo1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelEixo1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelEixo1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelEixo1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelEixo1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelEixo1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelEixo1.Name = "tableLayoutPanelEixo1";
            this.tableLayoutPanelEixo1.RowCount = 5;
            this.tableLayoutPanelEixo1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelEixo1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelEixo1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelEixo1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelEixo1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelEixo1.Size = new System.Drawing.Size(135, 336);
            this.tableLayoutPanelEixo1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanelControleEventos, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(628, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(248, 374);
            this.tableLayoutPanel1.TabIndex = 31;
            // 
            // tableLayoutPanelControleEventos
            // 
            this.tableLayoutPanelControleEventos.ColumnCount = 2;
            this.tableLayoutPanelControleEventos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelControleEventos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 89F));
            this.tableLayoutPanelControleEventos.Controls.Add(this.labeleventoid, 1, 1);
            this.tableLayoutPanelControleEventos.Controls.Add(this.buttonVer, 0, 0);
            this.tableLayoutPanelControleEventos.Controls.Add(this.buttonApagar, 0, 1);
            this.tableLayoutPanelControleEventos.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanelControleEventos.Location = new System.Drawing.Point(3, 271);
            this.tableLayoutPanelControleEventos.Name = "tableLayoutPanelControleEventos";
            this.tableLayoutPanelControleEventos.RowCount = 3;
            this.tableLayoutPanelControleEventos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanelControleEventos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanelControleEventos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanelControleEventos.Size = new System.Drawing.Size(181, 100);
            this.tableLayoutPanelControleEventos.TabIndex = 13;
            // 
            // labeleventoid
            // 
            this.labeleventoid.AutoSize = true;
            this.labeleventoid.Location = new System.Drawing.Point(95, 33);
            this.labeleventoid.Name = "labeleventoid";
            this.labeleventoid.Size = new System.Drawing.Size(0, 13);
            this.labeleventoid.TabIndex = 18;
            // 
            // buttonVer
            // 
            this.buttonVer.Location = new System.Drawing.Point(3, 3);
            this.buttonVer.Name = "buttonVer";
            this.buttonVer.Size = new System.Drawing.Size(75, 23);
            this.buttonVer.TabIndex = 14;
            this.buttonVer.Text = "Ver";
            this.buttonVer.UseVisualStyleBackColor = true;
            this.buttonVer.Click += new System.EventHandler(this.buttonVer_Click);
            // 
            // buttonApagar
            // 
            this.buttonApagar.Location = new System.Drawing.Point(3, 36);
            this.buttonApagar.Name = "buttonApagar";
            this.buttonApagar.Size = new System.Drawing.Size(75, 23);
            this.buttonApagar.TabIndex = 13;
            this.buttonApagar.Text = "Apagar";
            this.buttonApagar.UseVisualStyleBackColor = true;
            this.buttonApagar.Click += new System.EventHandler(this.buttonApagar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(95, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "id:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(242, 262);
            this.dataGridView1.TabIndex = 14;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.MouseEnter += new System.EventHandler(this.dataGridView1_MouseEnter);
            this.dataGridView1.MouseLeave += new System.EventHandler(this.dataGridView1_MouseLeave);
            // 
            // mySqlConnectionBindingSource
            // 
            this.mySqlConnectionBindingSource.DataSource = typeof(MySql.Data.MySqlClient.MySqlConnection);
            // 
            // Controles
            // 
            this.Controles.AutoSize = true;
            this.Controles.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controles.Controls.Add(this.labelPonto);
            this.Controles.Controls.Add(this.label9);
            this.Controles.Controls.Add(this.labelNomeProjeto);
            this.Controles.Controls.Add(this.label1);
            this.Controles.Controls.Add(this.button10);
            this.Controles.Controls.Add(this.currentPositionStringB);
            this.Controles.Controls.Add(this.buttonMover);
            this.Controles.Controls.Add(this.label4);
            this.Controles.Controls.Add(this.Velocidade2);
            this.Controles.Controls.Add(this.Velocidade1);
            this.Controles.Controls.Add(this.currentPositionStringA);
            this.Controles.Controls.Add(this.numericUpDown1);
            this.Controles.Controls.Add(this.label5);
            this.Controles.Controls.Add(this.numericUpDown2);
            this.Controles.Controls.Add(this.buttonPlayBackward);
            this.Controles.Controls.Add(this.buttonPlayForward);
            this.Controles.Controls.Add(this.buttonPause);
            this.Controles.Controls.Add(this.buttonPlay);
            this.Controles.Location = new System.Drawing.Point(3, 383);
            this.Controles.Name = "Controles";
            this.Controles.Size = new System.Drawing.Size(458, 101);
            this.Controles.TabIndex = 30;
            this.Controles.TabStop = false;
            this.Controles.Text = "Controles de Velocidade";
            // 
            // labelPonto
            // 
            this.labelPonto.AutoSize = true;
            this.labelPonto.Location = new System.Drawing.Point(439, 37);
            this.labelPonto.Name = "labelPonto";
            this.labelPonto.Size = new System.Drawing.Size(35, 13);
            this.labelPonto.TabIndex = 34;
            this.labelPonto.Text = "Ponto";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(360, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Ponto:";
            // 
            // labelNomeProjeto
            // 
            this.labelNomeProjeto.AutoSize = true;
            this.labelNomeProjeto.Location = new System.Drawing.Point(439, 16);
            this.labelNomeProjeto.Name = "labelNomeProjeto";
            this.labelNomeProjeto.Size = new System.Drawing.Size(71, 13);
            this.labelNomeProjeto.TabIndex = 32;
            this.labelNomeProjeto.Text = "Nome Projeto";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(360, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "Nome Projeto:";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(226, 27);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(56, 23);
            this.button10.TabIndex = 30;
            this.button10.Text = "button10";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // currentPositionStringB
            // 
            this.currentPositionStringB.AutoSize = true;
            this.currentPositionStringB.Location = new System.Drawing.Point(304, 37);
            this.currentPositionStringB.Name = "currentPositionStringB";
            this.currentPositionStringB.Size = new System.Drawing.Size(26, 13);
            this.currentPositionStringB.TabIndex = 29;
            this.currentPositionStringB.Text = "time";
            // 
            // buttonMover
            // 
            this.buttonMover.Location = new System.Drawing.Point(6, 24);
            this.buttonMover.Name = "buttonMover";
            this.buttonMover.Size = new System.Drawing.Size(52, 23);
            this.buttonMover.TabIndex = 27;
            this.buttonMover.Text = "mover";
            this.buttonMover.UseVisualStyleBackColor = true;
            this.buttonMover.Click += new System.EventHandler(this.buttonMover_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Segundos";
            // 
            // Velocidade2
            // 
            this.Velocidade2.AutoSize = true;
            this.Velocidade2.Location = new System.Drawing.Point(92, 34);
            this.Velocidade2.Name = "Velocidade2";
            this.Velocidade2.Size = new System.Drawing.Size(26, 13);
            this.Velocidade2.TabIndex = 21;
            this.Velocidade2.Text = "time";
            // 
            // Velocidade1
            // 
            this.Velocidade1.AutoSize = true;
            this.Velocidade1.Location = new System.Drawing.Point(92, 16);
            this.Velocidade1.Name = "Velocidade1";
            this.Velocidade1.Size = new System.Drawing.Size(26, 13);
            this.Velocidade1.TabIndex = 22;
            this.Velocidade1.Text = "time";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(266, 63);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(46, 20);
            this.numericUpDown1.TabIndex = 23;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(185, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "1X";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.DecimalPlaces = 1;
            this.numericUpDown2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDown2.Location = new System.Drawing.Point(6, 66);
            this.numericUpDown2.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(46, 20);
            this.numericUpDown2.TabIndex = 24;
            this.numericUpDown2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // buttonPlayBackward
            // 
            this.buttonPlayBackward.Location = new System.Drawing.Point(86, 63);
            this.buttonPlayBackward.Name = "buttonPlayBackward";
            this.buttonPlayBackward.Size = new System.Drawing.Size(39, 23);
            this.buttonPlayBackward.TabIndex = 17;
            this.buttonPlayBackward.Text = "<<";
            this.buttonPlayBackward.UseVisualStyleBackColor = true;
            this.buttonPlayBackward.Click += new System.EventHandler(this.buttonPlayBackward_Click);
            // 
            // buttonPlayForward
            // 
            this.buttonPlayForward.Location = new System.Drawing.Point(221, 63);
            this.buttonPlayForward.Name = "buttonPlayForward";
            this.buttonPlayForward.Size = new System.Drawing.Size(39, 23);
            this.buttonPlayForward.TabIndex = 20;
            this.buttonPlayForward.Text = ">>";
            this.buttonPlayForward.UseVisualStyleBackColor = true;
            this.buttonPlayForward.Click += new System.EventHandler(this.buttonPlayForward_Click);
            // 
            // buttonPause
            // 
            this.buttonPause.Location = new System.Drawing.Point(131, 63);
            this.buttonPause.Name = "buttonPause";
            this.buttonPause.Size = new System.Drawing.Size(39, 23);
            this.buttonPause.TabIndex = 3;
            this.buttonPause.Text = "& II";
            this.buttonPause.UseVisualStyleBackColor = true;
            this.buttonPause.Click += new System.EventHandler(this.buttonPause_Click);
            // 
            // buttonPlay
            // 
            this.buttonPlay.Location = new System.Drawing.Point(176, 63);
            this.buttonPlay.Name = "buttonPlay";
            this.buttonPlay.Size = new System.Drawing.Size(39, 23);
            this.buttonPlay.TabIndex = 4;
            this.buttonPlay.Text = ">";
            this.buttonPlay.UseVisualStyleBackColor = true;
            this.buttonPlay.Click += new System.EventHandler(this.buttonPlay_Click);
            // 
            // tableLayoutPanelGeral
            // 
            this.tableLayoutPanelGeral.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelGeral.AutoSize = true;
            this.tableLayoutPanelGeral.ColumnCount = 3;
            this.tableLayoutPanelGeral.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.86924F));
            this.tableLayoutPanelGeral.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.43838F));
            this.tableLayoutPanelGeral.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.78645F));
            this.tableLayoutPanelGeral.Controls.Add(this.Controles, 0, 1);
            this.tableLayoutPanelGeral.Controls.Add(this.tableLayoutPanel1, 2, 0);
            this.tableLayoutPanelGeral.Controls.Add(this.tabControl1, 1, 0);
            this.tableLayoutPanelGeral.Controls.Add(this.tableLayoutPanelVideo, 0, 0);
            this.tableLayoutPanelGeral.Controls.Add(this.tableLayoutPanel2, 1, 1);
            this.tableLayoutPanelGeral.Controls.Add(this.buttonEvento, 2, 1);
            this.tableLayoutPanelGeral.Location = new System.Drawing.Point(12, 41);
            this.tableLayoutPanelGeral.Name = "tableLayoutPanelGeral";
            this.tableLayoutPanelGeral.RowCount = 2;
            this.tableLayoutPanelGeral.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 78.17048F));
            this.tableLayoutPanelGeral.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.82952F));
            this.tableLayoutPanelGeral.Size = new System.Drawing.Size(879, 487);
            this.tableLayoutPanelGeral.TabIndex = 29;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonvideo2);
            this.groupBox1.Controls.Add(this.videoB);
            this.groupBox1.Controls.Add(this.vidoe1);
            this.groupBox1.Location = new System.Drawing.Point(32, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(202, 38);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Exibir";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(79, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "segundos";
            // 
            // numericUpDowndelay
            // 
            this.numericUpDowndelay.DecimalPlaces = 1;
            this.numericUpDowndelay.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDowndelay.Location = new System.Drawing.Point(27, 12);
            this.numericUpDowndelay.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDowndelay.Name = "numericUpDowndelay";
            this.numericUpDowndelay.Size = new System.Drawing.Size(46, 20);
            this.numericUpDowndelay.TabIndex = 29;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.buttonSincronizarVideo);
            this.groupBox2.Controls.Add(this.numericUpDowndelay);
            this.groupBox2.Location = new System.Drawing.Point(342, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(250, 38);
            this.groupBox2.TabIndex = 35;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sincornizio do Video A e B";
            // 
            // buttonSalvarImagem
            // 
            this.buttonSalvarImagem.Location = new System.Drawing.Point(607, 9);
            this.buttonSalvarImagem.Name = "buttonSalvarImagem";
            this.buttonSalvarImagem.Size = new System.Drawing.Size(75, 23);
            this.buttonSalvarImagem.TabIndex = 36;
            this.buttonSalvarImagem.Text = "Salvar Imagem";
            this.buttonSalvarImagem.UseVisualStyleBackColor = true;
            // 
            // checkBox_exapandir
            // 
            this.checkBox_exapandir.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.checkBox_exapandir.AutoSize = true;
            this.checkBox_exapandir.Location = new System.Drawing.Point(712, 18);
            this.checkBox_exapandir.Name = "checkBox_exapandir";
            this.checkBox_exapandir.Size = new System.Drawing.Size(112, 17);
            this.checkBox_exapandir.TabIndex = 37;
            this.checkBox_exapandir.Text = "Expandir a Tabela";
            this.checkBox_exapandir.UseVisualStyleBackColor = true;
            // 
            // ProjetoEventos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(927, 562);
            this.Controls.Add(this.checkBox_exapandir);
            this.Controls.Add(this.buttonSalvarImagem);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tableLayoutPanelGeral);
            this.KeyPreview = true;
            this.Name = "ProjetoEventos";
            this.Text = "Gravador de Eventos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ProjetoEventos_KeyDown);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanelVideo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tableLayoutPanelClassificacao1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanelClassificacao2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanelControleEventos.ResumeLayout(false);
            this.tableLayoutPanelControleEventos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mySqlConnectionBindingSource)).EndInit();
            this.Controles.ResumeLayout(false);
            this.Controles.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.tableLayoutPanelGeral.ResumeLayout(false);
            this.tableLayoutPanelGeral.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDowndelay)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonSincronizarVideo;
        private System.Windows.Forms.Label currentPositionStringA;
        private System.Windows.Forms.Timer timercontador;
        private System.Windows.Forms.Timer timerbackforward;
        private System.Windows.Forms.Button vidoe1;
        private System.Windows.Forms.Button buttonvideo2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button videoB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button buttonEvento;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelcurrentPositionA;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelVeiculo;
        private System.Windows.Forms.Label labelVeiculoCampo;
        private System.Windows.Forms.Label labelEixoCampo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelVideo;
        private AxWMPLib.AxWindowsMediaPlayer axWindowsMediaPlayer2;
        private AxWMPLib.AxWindowsMediaPlayer axWindowsMediaPlayer1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelClassificacao1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelVeiculo1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelVeiculo2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelVeiculo3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelClassificacao2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelEixo1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelControleEventos;
        private System.Windows.Forms.Button buttonVer;
        private System.Windows.Forms.Button buttonApagar;
        private System.Windows.Forms.GroupBox Controles;
        private System.Windows.Forms.Button buttonMover;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Velocidade2;
        private System.Windows.Forms.Label Velocidade1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Button buttonPlayBackward;
        private System.Windows.Forms.Button buttonPlayForward;
        private System.Windows.Forms.Button buttonPause;
        private System.Windows.Forms.Button buttonPlay;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelGeral;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDowndelay;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelcurrentPositionB;
        private System.Windows.Forms.Label currentPositionStringB;
        private System.Windows.Forms.Button buttonSalvarImagem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource mySqlConnectionBindingSource;
        private System.Windows.Forms.Label labelPonto;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelNomeProjeto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox_exapandir;
        private System.Windows.Forms.Label labeleventoid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}

